package com.progmasters.fundraiser.validation;

import com.progmasters.fundraiser.domain.Project;
import com.progmasters.fundraiser.dto.in.ProjectForm;
import com.progmasters.fundraiser.repository.ProjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.time.LocalDate;
import java.util.Optional;

@Component
public class ProjectFormValidator implements Validator {

    private static final Logger logger = LoggerFactory.getLogger(AccountRegistrationCommandValidator.class);

    private ProjectRepository projectRepository;

    @Autowired
    public ProjectFormValidator(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return ProjectForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ProjectForm form = (ProjectForm) target;
        Optional<Project> project;
        if (form.getId() != null) {
            project = projectRepository.findById(form.getId());

            if (!project.isPresent()) {
                errors.rejectValue("name", "project.null");
            } else if (form.getDeadline().isBefore(LocalDate.now())) {
                errors.rejectValue("deadline", "date.invalid");
            }
        }
    }
}
