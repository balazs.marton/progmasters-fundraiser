/*
 * Copyright © Progmasters (QTC Kft.), 2018.
 * All rights reserved. No part or the whole of this Teaching Material (TM) may be reproduced, copied, distributed,
 * publicly performed, disseminated to the public, adapted or transmitted in any form or by any means, including
 * photocopying, recording, or other electronic or mechanical methods, without the prior written permission of QTC Kft.
 * This TM may only be used for the purposes of teaching exclusively by QTC Kft. and studying exclusively by QTC Kft.’s
 * students and for no other purposes by any parties other than QTC Kft.
 * This TM shall be kept confidential and shall not be made public or made available or disclosed to any unauthorized person.
 * Any dispute or claim arising out of the breach of these provisions shall be governed by and construed in accordance with the laws of Hungary.
 */

package com.progmasters.fundraiser.validation;

import com.progmasters.fundraiser.domain.Account;
import com.progmasters.fundraiser.dto.in.TransferCreationCommand;
import com.progmasters.fundraiser.service.AccountService;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.servlet.http.HttpServletRequest;

@Component
public class TransferCreationCommandValidator implements Validator {

    private AccountService accountService;

    private HttpServletRequest request;

    public TransferCreationCommandValidator(AccountService accountService, HttpServletRequest request) {
        this.accountService = accountService;
        this.request = request;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return TransferCreationCommand.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        TransferCreationCommand transferCreationCommand = (TransferCreationCommand) target;
        Account from = accountService.findByUserName(request.getRemoteUser());

        if (transferCreationCommand.getAmount() == null) {
            errors.rejectValue("amount", "transfer.amount.missing");
        } else {
            int transferAmount = transferCreationCommand.getAmount();

            if (from.getBalance() - transferAmount < 0) {
                errors.rejectValue("amount", "account.balanceNotEnough");
            } else if (transferAmount > 1000 || transferAmount < 50) {
                errors.rejectValue("amount", "transfer.amount.boundaries");
            }
        }
        /*
        if (!transferCreationCommand.getTimeStamp().equals("")) {
            String transactionTime = transferCreationCommand.getTimeStamp();
            LocalDateTime formatDateTime = LocalDateTime.parse(transactionTime);

            if (formatDateTime.isBefore(LocalDateTime.now())) {
                errors.rejectValue("timeStamp", "transfer.time.boundaries");
            }


        }
        */
    }
}

