/*
 * Copyright © Progmasters (QTC Kft.), 2018.
 * All rights reserved. No part or the whole of this Teaching Material (TM) may be reproduced, copied, distributed,
 * publicly performed, disseminated to the public, adapted or transmitted in any form or by any means, including
 * photocopying, recording, or other electronic or mechanical methods, without the prior written permission of QTC Kft.
 * This TM may only be used for the purposes of teaching exclusively by QTC Kft. and studying exclusively by QTC Kft.’s
 * students and for no other purposes by any parties other than QTC Kft.
 * This TM shall be kept confidential and shall not be made public or made available or disclosed to any unauthorized person.
 * Any dispute or claim arising out of the breach of these provisions shall be governed by and construed in accordance with the laws of Hungary.
 */

package com.progmasters.fundraiser.validation;

import com.progmasters.fundraiser.dto.in.AccountRegistrationCommand;
import com.progmasters.fundraiser.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.servlet.http.HttpServletRequest;

@Component
public class AccountRegistrationCommandValidator implements Validator {

    private static final Logger logger = LoggerFactory.getLogger(AccountRegistrationCommandValidator.class);

    private AccountService accountService;

    private HttpServletRequest request;

    public AccountRegistrationCommandValidator(AccountService accountService, HttpServletRequest request) {
        this.accountService = accountService;
        this.request = request;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return AccountRegistrationCommand.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        AccountRegistrationCommand command = (AccountRegistrationCommand) target;
        command.getEmail();

        if (accountService.findByEmail(command.getEmail()) != null){
            errors.rejectValue("email", "email.already.used");
        }
        if (accountService.findByUserName(command.getUserName()) != null){
            errors.rejectValue("userName", "user.already.used");
        }
        if (!command.getPassword().equals(command.getConfirmPassword())) {
            errors.rejectValue("confirmPassword", "confirm.password.invalid");
        }
    }
}
