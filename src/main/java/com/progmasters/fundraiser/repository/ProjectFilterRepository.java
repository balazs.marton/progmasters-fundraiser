package com.progmasters.fundraiser.repository;

import com.progmasters.fundraiser.dto.out.ProjectListItem;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDate;
import java.util.List;

@Repository
@Transactional
public class ProjectFilterRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<ProjectListItem> getProjectsByCategory(Long categoryId, String remainingTime, String funds) {
        String[] days = new String[0];
        if (remainingTime != null) {
            days = remainingTime.split("[\\D]+");
        }

        String[] boundaries = new String[0];
        if (funds != null) {
            boundaries = funds.split("[\\D]+");
        }

        String jpql = "SELECT new com.progmasters.fundraiser.dto.out.ProjectListItem(p) FROM Project p where 1=1 ";
        if (categoryId != null) {
            jpql += " AND p.category.id = :categoryId";
        }
        if (remainingTime != null) {
            if (days.length == 2) {
                jpql += " AND p.deadline BETWEEN :minDays AND :maxDays";
            } else {
                if (Long.parseLong(days[0]) != 0) {
                    jpql += " AND p.deadline >= :minDays";
                } else {
                    jpql += " AND p.deadline <= :minDays";
                }
            }
        }
        if (funds != null) {
            if (boundaries.length == 2) {
                jpql += " AND p.funds BETWEEN ((p.goalFunds / 100) * :minFunds) AND ((p.goalFunds / 100) * :maxFunds)";
            } else {
                jpql += " AND p.funds >= ((p.goalFunds / 100) * :minFunds)";
            }
        }

        Query query = entityManager.createQuery(jpql, ProjectListItem.class);

        if (categoryId != null) {
            query.setParameter("categoryId", categoryId);
        }
        if (remainingTime != null) {
            if (days.length == 2) {
                query.setParameter("minDays", LocalDate.now().plusDays(Long.parseLong(days[0])))
                        .setParameter("maxDays", LocalDate.now().plusDays(Long.parseLong(days[1])));
            } else {
                query.setParameter("minDays", LocalDate.now().plusDays(Long.parseLong(days[0])));
            }
        }
        if (funds != null) {
            if (boundaries.length == 2) {
                query.setParameter("minFunds", (int) Long.parseLong(boundaries[0]))
                        .setParameter("maxFunds", (int) Long.parseLong(boundaries[1]));
            } else {
                query.setParameter("minFunds", (int) Long.parseLong(boundaries[0]));
            }
        }

        return query.getResultList();
    }
}
