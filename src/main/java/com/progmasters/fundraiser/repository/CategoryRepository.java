package com.progmasters.fundraiser.repository;

import com.progmasters.fundraiser.domain.Category;
import com.progmasters.fundraiser.dto.out.CategoryDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query("SELECT new com.progmasters.fundraiser.dto.out.CategoryDetails(c) FROM Category c")
    List<CategoryDetails> findAllCategoryDetails();

    @Query("SELECT new com.progmasters.fundraiser.dto.out.CategoryDetails(c) FROM Category c" +
            " WHERE NOT size(c.projects) = 0")
    List<CategoryDetails> findUsedCategories();
}
