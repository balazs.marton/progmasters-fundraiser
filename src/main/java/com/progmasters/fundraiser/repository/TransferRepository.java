/*
 * Copyright © Progmasters (QTC Kft.), 2018.
 * All rights reserved. No part or the whole of this Teaching Material (TM) may be reproduced, copied, distributed,
 * publicly performed, disseminated to the public, adapted or transmitted in any form or by any means, including
 * photocopying, recording, or other electronic or mechanical methods, without the prior written permission of QTC Kft.
 * This TM may only be used for the purposes of teaching exclusively by QTC Kft. and studying exclusively by QTC Kft.’s
 * students and for no other purposes by any parties other than QTC Kft.
 * This TM shall be kept confidential and shall not be made public or made available or disclosed to any unauthorized person.
 * Any dispute or claim arising out of the breach of these provisions shall be governed by and construed in accordance with the laws of Hungary.
 */

package com.progmasters.fundraiser.repository;

import com.progmasters.fundraiser.domain.Transfer;
import com.progmasters.fundraiser.dto.out.TransferGroupByDayListItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransferRepository extends JpaRepository<Transfer, Long> {

    @Query(" select " +
            " new com.progmasters.fundraiser.dto.out.TransferGroupByDayListItem(function('date_format', t.transferDate, '%Y-%m-%d'), SUM(t.amount)) " +
            " from Transfer t where t.project.id =:projectId " +
            "  GROUP BY function('date_format', t.transferDate, '%Y-%m-%d') ")
    List<TransferGroupByDayListItem> getTransferGroupByDay(@Param("projectId") Long projectId);

//    List<Transfer> findAllByFrom(Account from);

//    List<Transfer> findAllByTo(Account to);


}
