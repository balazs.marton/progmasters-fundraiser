package com.progmasters.fundraiser.repository;

import com.progmasters.fundraiser.domain.Project;
import com.progmasters.fundraiser.dto.out.ProjectListItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    @Query("select new com.progmasters.fundraiser.dto.out.ProjectListItem(p) from Project p")
    List<ProjectListItem> getAllProject();

    @Query("select new com.progmasters.fundraiser.dto.out.ProjectListItem(p) from Project p" +
            " where p.account.id =:accId")
    List<ProjectListItem> getAllProjectsByAccountId(@Param("accId") Long accId);

    @Query("SELECT p FROM Project p WHERE p.category.id = :id")
    List<Project> findAllByCategory(@Param("id") Long id);

    @Query("SELECT p FROM Project p WHERE p.account.id = :id")
    List<Project> findAllByAccountId(@Param("id") Long id);

    @Query("SELECT p FROM Project p")
    List<Project> findAllByFilter(@Param("query") String query);
}
