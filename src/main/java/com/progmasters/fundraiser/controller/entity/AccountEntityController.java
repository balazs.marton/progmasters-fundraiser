package com.progmasters.fundraiser.controller.entity;

import com.progmasters.fundraiser.domain.Account;
import com.progmasters.fundraiser.dto.in.AccountRegistrationCommand;
import com.progmasters.fundraiser.service.AccountService;
import com.progmasters.fundraiser.service.EmailServiceImpl;
import com.progmasters.fundraiser.validation.AccountRegistrationCommandValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/account/entity")
public class AccountEntityController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountEntityController.class);

    private AccountService accountService;
    private AccountRegistrationCommandValidator accountRegistrationCommandValidator;



    @Autowired
    public AccountEntityController(AccountService accountService, AccountRegistrationCommandValidator accountRegistrationCommandValidator) {
        this.accountService = accountService;
        this.accountRegistrationCommandValidator = accountRegistrationCommandValidator;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(accountRegistrationCommandValidator);
    }


    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, path = "/register")
    public ResponseEntity<?> registerNewAccount(@RequestBody @Valid AccountRegistrationCommand accountRegistrationCommand, HttpServletRequest request) {
        LOGGER.info("Registration request from {}", request.getRemoteAddr());
        return new ResponseEntity<>(accountService.registerAccount(accountRegistrationCommand, request), HttpStatus.CREATED);
    }
}
