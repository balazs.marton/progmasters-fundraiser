package com.progmasters.fundraiser.controller.entity;

import com.progmasters.fundraiser.dto.in.ProjectForm;
import com.progmasters.fundraiser.service.ImageService;
import com.progmasters.fundraiser.service.ProjectService;
import com.progmasters.fundraiser.validation.ProjectFormValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/project/entity")
public class ProjectEntityController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectEntityController.class);

    private ProjectService projectService;
    private ProjectFormValidator projectFormValidator;
    private ImageService imageService;

    @Autowired
    public ProjectEntityController(ProjectService projectService, ProjectFormValidator projectFormValidator, ImageService imageService) {
        this.projectService = projectService;
        this.projectFormValidator = projectFormValidator;
        this.imageService = imageService;
    }

    @InitBinder("projectForm")
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(projectFormValidator);
    }

    @PostMapping("/create-project")
    public ResponseEntity createProject(@RequestBody @Valid ProjectForm projectForm, Principal principal) {
        projectService.create(projectForm, principal);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/image")
    public ResponseEntity<?> uploadRoomImage(@RequestParam("file") MultipartFile uploadfile) {

        if (uploadfile.isEmpty()) {
            return new ResponseEntity<>("please select a file!", HttpStatus.OK);
        }
        List<String> imagePaths = imageService.saveUploadedFiles(Arrays.asList(uploadfile));
        return new ResponseEntity<>(imagePaths, HttpStatus.OK);

    }

    @PutMapping("/update-project")
    public ResponseEntity updateProject(@RequestBody @Valid ProjectForm projectForm) {
        projectService.update(projectForm);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteProject(@PathVariable Long id) {
        projectService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
