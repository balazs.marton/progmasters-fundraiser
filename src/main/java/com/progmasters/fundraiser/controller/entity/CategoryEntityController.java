package com.progmasters.fundraiser.controller.entity;

import com.progmasters.fundraiser.domain.Category;
import com.progmasters.fundraiser.dto.in.CategoryForm;
import com.progmasters.fundraiser.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/category/entity")
public class CategoryEntityController {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryEntityController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @PostMapping
    public ResponseEntity<?> createCategory(@RequestBody @Valid CategoryForm categoryForm){
        Category savedCategory = new Category(categoryForm);
        categoryRepository.save(savedCategory);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
