package com.progmasters.fundraiser.controller.entity;

import com.progmasters.fundraiser.domain.Account;
import com.progmasters.fundraiser.dto.in.AccountRegistrationCommand;
import com.progmasters.fundraiser.dto.in.EmailForm;
import com.progmasters.fundraiser.dto.out.EmailDetails;
import com.progmasters.fundraiser.service.AccountService;
import com.progmasters.fundraiser.service.EmailServiceImpl;
import com.progmasters.fundraiser.validation.AccountRegistrationCommandValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/email/entity")
public class EmailEntityController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailEntityController.class);

    private EmailServiceImpl emailService;

    @Autowired
    public EmailEntityController(EmailServiceImpl emailService) {
        this.emailService = emailService;
    }


    @PutMapping
    public ResponseEntity<?> registerNewAccount(@RequestBody @Valid EmailForm emailForm, HttpServletRequest request) {
        LOGGER.info("Update email text from {}", request.getRemoteAddr());

        emailService.updateEmail(emailForm);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
