package com.progmasters.fundraiser.controller.entity;

import com.progmasters.fundraiser.dto.in.TransferCreationCommand;
import com.progmasters.fundraiser.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/api/transfer/entity")
public class TransferEntityController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransferEntityController.class);
    private TransferService transferService;

    @Autowired
    public TransferEntityController(TransferService transferService) {
        this.transferService = transferService;
    }

    @PostMapping
    public ResponseEntity<?> saveTransfer(@RequestBody @Valid TransferCreationCommand transferCreationCommand, Principal principal) {
        LOGGER.debug("transfer created {}", principal.getName());
        transferService.createTransfer(transferCreationCommand, principal);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
