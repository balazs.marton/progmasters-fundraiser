package com.progmasters.fundraiser.controller.list;

import com.progmasters.fundraiser.dto.out.TransferGroupByDayListItem;
import com.progmasters.fundraiser.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/transfer/list")
public class TransferListController {

    private TransferService transferService;

    @Autowired
    public TransferListController(TransferService transferService) {
        this.transferService = transferService;
    }

    @GetMapping("/{projectId}")
    public List<TransferGroupByDayListItem> getTransfersByProjectGroupByDay(@PathVariable Long projectId){
        return transferService.getTransfersByProjectGroupByDay(projectId);
    }
}
