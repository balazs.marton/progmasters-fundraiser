package com.progmasters.fundraiser.controller.list;

import com.progmasters.fundraiser.dto.out.ProjectDetailsWithCalculatedValues;
import com.progmasters.fundraiser.dto.out.ProjectListItem;
import com.progmasters.fundraiser.repository.ProjectFilterRepository;
import com.progmasters.fundraiser.dto.out.ProjectsMapItem;
import com.progmasters.fundraiser.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/project/list")
public class ProjectListController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectListController.class);

    private ProjectService projectService;
    private ProjectFilterRepository filterService;

    @Autowired
    public ProjectListController(ProjectService projectService, ProjectFilterRepository filterService) {
        this.projectService = projectService;
        this.filterService = filterService;
    }

    @GetMapping
    public List<ProjectListItem> getProjects() {
        return projectService.getAllProject();
    }

    @GetMapping("/{projectId}")
    public ProjectDetailsWithCalculatedValues getProjectDetails(@PathVariable Long projectId, Principal principal) {
        return projectService.getProject(projectId, principal);
    }

    @GetMapping("/filter")
    public List<ProjectListItem> getProjectsByCategory(@RequestParam(name = "category", required = false) Long categoryId,
                                                       @RequestParam(name = "remainingTime", required = false) String remainingTime,
                                                       @RequestParam(name = "funds", required = false) String funds) {
        return filterService.getProjectsByCategory(categoryId, remainingTime, funds);
    }

    @GetMapping("/inMap")
    public ProjectsMapItem getProjectsInMap(){
        return projectService.getProjectsInMap();
    }

}
