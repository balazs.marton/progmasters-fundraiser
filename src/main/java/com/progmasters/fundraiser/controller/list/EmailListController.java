package com.progmasters.fundraiser.controller.list;

import com.progmasters.fundraiser.dto.out.AccountDetails;
import com.progmasters.fundraiser.dto.out.EmailDetails;
import com.progmasters.fundraiser.security.AuthenticatedAccountDetails;
import com.progmasters.fundraiser.service.AccountService;
import com.progmasters.fundraiser.service.EmailServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/email/list")
public class EmailListController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailListController.class);

    private EmailServiceImpl emailService;

    @Autowired
    public EmailListController(EmailServiceImpl emailService) {
        this.emailService = emailService;
    }

    @GetMapping
    public ResponseEntity<?> getAllEmails() {
        return new ResponseEntity<>(emailService.getAllAllEmails(), HttpStatus.OK);
    }
}
