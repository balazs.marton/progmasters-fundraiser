package com.progmasters.fundraiser.controller.list;

import com.progmasters.fundraiser.dto.out.AccountDetails;
import com.progmasters.fundraiser.security.AuthenticatedAccountDetails;
import com.progmasters.fundraiser.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/account/list")
public class AccountListController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountListController.class);

    private AccountService accountService;

    @Autowired
    public AccountListController(AccountService accountService) {
        this.accountService = accountService;
    }


    @GetMapping("/login")
    public ResponseEntity<AuthenticatedAccountDetails> getUserInfo() {
        return new ResponseEntity<>(accountService.login(SecurityContextHolder.getContext().getAuthentication()), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<AccountDetails>> getAllAccounts() {
        return new ResponseEntity<>(accountService.getAllAccounts(), HttpStatus.OK);
    }

    @GetMapping("/myAccountDetails")
    public ResponseEntity<AccountDetails> getMyAccountDetails(HttpServletRequest request, Principal principal) {
        AccountDetails accountDetails = accountService.getMyAccountDetails(principal);

        if (accountDetails != null) {
            LOGGER.info("Account details request from {}", request.getRemoteAddr());
            return new ResponseEntity<>(accountDetails, HttpStatus.OK);
        } else {
            LOGGER.debug("Account not found for IP address: " + request.getRemoteAddr());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
