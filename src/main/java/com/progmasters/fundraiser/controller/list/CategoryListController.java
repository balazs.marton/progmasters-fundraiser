package com.progmasters.fundraiser.controller.list;

import com.progmasters.fundraiser.dto.out.CategoryDetails;
import com.progmasters.fundraiser.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/category/list")
public class CategoryListController {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryListController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @GetMapping
    public List<CategoryDetails> getCategories() {
        return categoryRepository.findAllCategoryDetails();
    }

    @GetMapping("/is-used")
    public List<CategoryDetails> getUsedCategories() {
        return categoryRepository.findUsedCategories();
    }

}
