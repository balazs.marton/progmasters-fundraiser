package com.progmasters.fundraiser.domain;

import com.progmasters.fundraiser.dto.in.ProjectForm;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    private String imageUrl;

    private Integer funds;

    @ManyToOne
    private Account account;

    private Integer goalFunds;

    private LocalDate deadline;

    private LocalDate createDate;

    @ManyToOne
    private Category category;

    public Project() {
    }

    public Project(ProjectForm projectDetails) {
        this.id = projectDetails.getId();
        this.name = projectDetails.getName();
        this.description = projectDetails.getDescription();
        this.imageUrl = projectDetails.getImageUrl();
        this.funds = 0;
        this.goalFunds = projectDetails.getGoalFunds();
        this.deadline = projectDetails.getDeadline();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getFunds() {
        return funds;
    }

    public void setFunds(Integer funds) {
        this.funds = funds;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Integer getGoalFunds() {
        return goalFunds;
    }

    public void setGoalFunds(Integer goalFunds) {
        this.goalFunds = goalFunds;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }
}
