package com.progmasters.fundraiser.domain;

public enum Currency {
    HUF(0.00361, "HUF"),
    EUR(1.167386, "EUR"),
    USD (1.0, "USD"),
    GBP(1.314138, "GBP");

    private Double exchangeRateToUsd;
    private String name;

    Currency(Double exchangeRateToUsd, String name) {
        this.exchangeRateToUsd = exchangeRateToUsd;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer exchangeToUsd(Integer amount){
        Double usd = amount*exchangeRateToUsd;
        return usd.intValue();
    }

    public Integer exchangeFromUsd(Integer amount){
        Double usd = amount/exchangeRateToUsd;
        return usd.intValue();
    }
}
