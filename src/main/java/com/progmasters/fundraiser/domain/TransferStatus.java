package com.progmasters.fundraiser.domain;

public enum TransferStatus {

    OK("Ok"),

    PENDING("Pending"),

    CONFIRMED("Confirmed"),

    FAILED("Failed");

    private String displayName;

    TransferStatus(String displayName) {

        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}


