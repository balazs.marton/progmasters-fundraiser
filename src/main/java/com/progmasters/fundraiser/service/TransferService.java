/*
 * Copyright © Progmasters (QTC Kft.), 2018.
 * All rights reserved. No part or the whole of this Teaching Material (TM) may be reproduced, copied, distributed,
 * publicly performed, disseminated to the public, adapted or transmitted in any form or by any means, including
 * photocopying, recording, or other electronic or mechanical methods, without the prior written permission of QTC Kft.
 * This TM may only be used for the purposes of teaching exclusively by QTC Kft. and studying exclusively by QTC Kft.’s
 * students and for no other purposes by any parties other than QTC Kft.
 * This TM shall be kept confidential and shall not be made public or made available or disclosed to any unauthorized person.
 * Any dispute or claim arising out of the breach of these provisions shall be governed by and construed in accordance with the laws of Hungary.
 */

package com.progmasters.fundraiser.service;

import com.progmasters.fundraiser.domain.*;
import com.progmasters.fundraiser.dto.in.TransferCreationCommand;
import com.progmasters.fundraiser.dto.out.TransferGroupByDayListItem;
import com.progmasters.fundraiser.repository.AccountRepository;
import com.progmasters.fundraiser.repository.ProjectRepository;
import com.progmasters.fundraiser.repository.TransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;


@Service
@EnableScheduling
@Transactional
public class TransferService {

    private AccountRepository accountRepository;
    private ProjectRepository projectRepository;
    private TransferRepository transferRepository;
    private EmailServiceImpl emailService;


    @Autowired
    public TransferService(AccountRepository accountRepository, ProjectRepository projectRepository, TransferRepository transferRepository, EmailServiceImpl emailService) {
        this.accountRepository = accountRepository;
        this.projectRepository = projectRepository;
        this.transferRepository = transferRepository;
        this.emailService = emailService;
    }

    public void createTransfer(TransferCreationCommand transferCreationCommand, Principal principal) {
        Account myAccount = accountRepository.findByUserName(principal.getName());
        Project project = projectRepository.findById(transferCreationCommand.getProjectId()).get();

        if(myAccount.getBalance() > transferCreationCommand.getAmount()){
            transferTransaction(transferCreationCommand, myAccount, project);
        }
    }

    private void transferTransaction(TransferCreationCommand transferCreationCommand, Account myAccount, Project project) {
        int transferAmount = getExchangeTransferAmount(transferCreationCommand.getAmount(), myAccount.getCurrency());

        subtractFromMyAccount(myAccount, transferAmount);

        transferToProject(project, transferAmount);

        Transfer createdTransfer = new Transfer(myAccount, project, transferAmount, LocalDateTime.now());

        emailService.sendMessageAboutTransfer(myAccount, transferAmount);

        accountRepository.save(myAccount);
        transferRepository.save(createdTransfer);
    }

    private void transferToProject(Project project, int transferAmount) {
        int actualProjectFunds = project.getFunds();
        project.setFunds(actualProjectFunds + transferAmount);
    }

    private void subtractFromMyAccount(Account myAccount, int transferAmount) {
        int myAccountActualBalance = myAccount.getBalance();
        myAccount.setBalance(myAccountActualBalance - transferAmount);
    }

    private int getExchangeTransferAmount(int amount, Currency myCurrency) {
        return myCurrency.exchangeToUsd(amount);
    }

    public List<TransferGroupByDayListItem> getTransfersByProjectGroupByDay(Long projectId) {
        return transferRepository.getTransferGroupByDay(projectId);
    }
}