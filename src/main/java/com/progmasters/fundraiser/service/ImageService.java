package com.progmasters.fundraiser.service;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ImageService {

    public List<String> saveUploadedFiles(List<MultipartFile> files) {
        List<String> imagePaths = new ArrayList<>();

        Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", "dup84qpgn",
                "api_key", "221473498278788",
                "api_secret", "XDUSfSO6gB-nftkZwQDuX9ln-q0"));

        for (MultipartFile file : files) {

            if (file.isEmpty()) {
                continue; //next pls
            }
            try {
                Map uploadResult = cloudinary.uploader().upload(file.getBytes(), ObjectUtils.emptyMap());

                String secureUrl = (String) uploadResult.get("secure_url");

                imagePaths.add(secureUrl);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return imagePaths;
    }
}
