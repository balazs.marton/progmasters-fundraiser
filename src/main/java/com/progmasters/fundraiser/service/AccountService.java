/*
 * Copyright © Progmasters (QTC Kft.), 2018.
 * All rights reserved. No part or the whole of this Teaching Material (TM) may be reproduced, copied, distributed,
 * publicly performed, disseminated to the public, adapted or transmitted in any form or by any means, including
 * photocopying, recording, or other electronic or mechanical methods, without the prior written permission of QTC Kft.
 * This TM may only be used for the purposes of teaching exclusively by QTC Kft. and studying exclusively by QTC Kft.’s
 * students and for no other purposes by any parties other than QTC Kft.
 * This TM shall be kept confidential and shall not be made public or made available or disclosed to any unauthorized person.
 * Any dispute or claim arising out of the breach of these provisions shall be governed by and construed in accordance with the laws of Hungary.
 */

package com.progmasters.fundraiser.service;

import com.progmasters.fundraiser.domain.Account;
import com.progmasters.fundraiser.domain.Currency;
import com.progmasters.fundraiser.dto.out.AccountDetails;
import com.progmasters.fundraiser.dto.in.AccountRegistrationCommand;
import com.progmasters.fundraiser.dto.out.ProjectListItem;
import com.progmasters.fundraiser.repository.AccountRepository;
import com.progmasters.fundraiser.security.AuthenticatedAccountDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class AccountService {

    private AccountRepository accountRepository;
    private PasswordEncoder passwordEncoder;
    private ProjectService projectService;
    private EmailServiceImpl emailService;

    @Autowired
    public AccountService(AccountRepository accountRepository, PasswordEncoder passwordEncoder, ProjectService projectService, EmailServiceImpl emailService) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
        this.projectService = projectService;
        this.emailService = emailService;
    }

    public Account registerAccount(@Valid AccountRegistrationCommand accountRegistrationCommand, HttpServletRequest request) {
        Account account = new Account(accountRegistrationCommand);
        account.setIpAddress(request.getRemoteAddr());
        String encodedPassword = passwordEncoder.encode(account.getPassword());
        account.setPassword(encodedPassword);
        Currency currency = Currency.valueOf(accountRegistrationCommand.getCurrency());
        account.setCurrency(currency);
        account.setBalance(5000);

        emailService.sendRegistrationMessage(account);

        return accountRepository.save(account);
    }

    public Account findByEmail(String email) {
        return accountRepository.findByEmail(email);
    }

    public Account findByUserName(String userName) {
        return accountRepository.findByUserName(userName);
    }


    public List<AccountDetails> getAllAccounts() {
        List<AccountDetails> accounts = new ArrayList<>();
        for (Account account : accountRepository.findAll()) {
            accounts.add(new AccountDetails(account));
        }
        Collections.sort(accounts);
        return accounts;
    }

    public AccountDetails getMyAccountDetails(Principal principal) {
        Account myAccount = accountRepository.findByUserName(principal.getName());

        if (myAccount != null) {
            List<ProjectListItem> myAccountProjects = projectService.getProjectsByAccountId(myAccount.getId());
            System.out.println("MY ACCOUNT:" + myAccountProjects.size());
            AccountDetails myAccountDetails = new AccountDetails(myAccount, myAccountProjects);
            return myAccountDetails;
        } else {
            return null;
        }
    }

    public AuthenticatedAccountDetails login(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return new AuthenticatedAccountDetails(userDetails);
    }
}
