package com.progmasters.fundraiser.service;

import com.progmasters.fundraiser.domain.Account;
import com.progmasters.fundraiser.domain.Currency;
import com.progmasters.fundraiser.domain.Project;
import com.progmasters.fundraiser.domain.Role;
import com.progmasters.fundraiser.dto.in.ProjectForm;
import com.progmasters.fundraiser.dto.out.ProjectDetailsWithCalculatedValues;
import com.progmasters.fundraiser.dto.out.ProjectListItem;
import com.progmasters.fundraiser.dto.out.ProjectsMapItem;
import com.progmasters.fundraiser.repository.AccountRepository;
import com.progmasters.fundraiser.repository.CategoryRepository;
import com.progmasters.fundraiser.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ProjectService {

    private ProjectRepository projectRepository;
    private AccountRepository accountRepository;
    private CategoryRepository categoryRepository;

    @Autowired
    public ProjectService(ProjectRepository projectRepository, AccountRepository accountRepository, CategoryRepository categoryRepository) {
        this.projectRepository = projectRepository;
        this.accountRepository = accountRepository;
        this.categoryRepository = categoryRepository;
    }

    public Project create(ProjectForm projectForm, Principal principal) {
        Account myAccount = accountRepository.findByUserName(principal.getName());
        projectForm.setUserId(myAccount.getId());
        Project project = new Project(projectForm);
        project.setCategory(categoryRepository.findById(projectForm.getCategoryId()).get());
        project.setAccount(myAccount);
        project.setCreateDate(LocalDate.now());

        return projectRepository.save(project);
    }

    public List<ProjectListItem> getAllProject() {
        List<ProjectListItem> projects = new ArrayList<>();

        for (Project project : projectRepository.findAll()) {
            ProjectListItem listItem = new ProjectListItem(project);

            listItem.setCurrency(Currency.USD.toString());

            projects.add(listItem);
        }

        return projects;
    }

    public ProjectDetailsWithCalculatedValues getProject(Long projectId, Principal principal) {
        Project project = projectRepository.findById(projectId).get();
        Account myAccount = null;
        ProjectDetailsWithCalculatedValues projectDetails = new ProjectDetailsWithCalculatedValues(project);

        if (principal != null) {
            myAccount = accountRepository.findByUserName(principal.getName());
        }

        if (myAccount != null && myAccount.getRole() == Role.ROLE_USER) {
            Currency myCurrency = myAccount.getCurrency();
            projectDetails.setCurrency(myCurrency.toString());
            projectDetails.setFunds(myCurrency.exchangeFromUsd(projectDetails.getFunds()));
            projectDetails.setGoal(myCurrency.exchangeFromUsd(projectDetails.getGoal()));
        } else {
            projectDetails.setCurrency(Currency.USD.toString());
        }

        return projectDetails;
    }

    public List<ProjectListItem> getProjectsByAccountId(Long accountId){
        List<ProjectListItem> projects = new ArrayList<>();

        Account myAccount = accountRepository.findById(accountId).get();

        for (Project project : projectRepository.findAllByAccountId(accountId)) {
            ProjectListItem listItem = new ProjectListItem(project);

            if (myAccount.getRole() == Role.ROLE_USER) {
                Currency myCurrency = myAccount.getCurrency();
                listItem.setCurrency(myCurrency.toString());
                listItem.setFunds(myCurrency.exchangeFromUsd(project.getFunds()));
                listItem.setGoalFunds(myCurrency.exchangeFromUsd(project.getGoalFunds()));
            } else {
                listItem.setCurrency(Currency.USD.toString());
            }

            projects.add(listItem);
        }

        return projects;
    }

    public Project update(ProjectForm projectForm) {
        Project project = projectRepository.findById(projectForm.getId()).get();
        project.setName(projectForm.getName());
        project.setDescription(projectForm.getDescription());
        project.setImageUrl(projectForm.getImageUrl());
        project.setGoalFunds(projectForm.getGoalFunds());
        project.setAccount(accountRepository.findById(projectForm.getUserId()).get());
        project.setDeadline(projectForm.getDeadline());
        project.setCategory(categoryRepository.findById(projectForm.getCategoryId()).get());

        return projectRepository.save(project);
    }

    public void delete(Long id) {
        projectRepository.delete(projectRepository.findById(id).get());
    }

    public List<ProjectListItem> getProjectsByCategory(Long id) {
        List<ProjectListItem> projects = new ArrayList<>();

        for (Project project : projectRepository.findAllByCategory(id)) {
            projects.add(new ProjectListItem(project));
        }

        return projects;
    }

    public ProjectsMapItem getProjectsInMap() {
        return new ProjectsMapItem(projectRepository.getAllProject());
    }
}
