package com.progmasters.fundraiser.service;

import com.progmasters.fundraiser.domain.Account;
import com.progmasters.fundraiser.domain.Email;
import com.progmasters.fundraiser.dto.in.EmailForm;
import com.progmasters.fundraiser.dto.out.EmailDetails;
import com.progmasters.fundraiser.repository.EmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
public class EmailServiceImpl {
    private JavaMailSender emailSender;
    private EmailRepository emailRepository;

    @Autowired
    public EmailServiceImpl(JavaMailSender emailSender, EmailRepository emailRepository) {
        this.emailSender = emailSender;
        this.emailRepository = emailRepository;
    }

    public void sendRegistrationMessage(Account account) {

        Email email = emailRepository.getEmailByName("Registration email");
        String to = account.getEmail();
        String emailText = replaceVariables(email.getEmailBody(), account);

        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setTo(to);
            messageHelper.setSubject(email.getEmailSubject());
            messageHelper.setText(emailText, true);
        };
        emailSender.send(messagePreparator);
    }

    private String replaceVariables(String emailText, Account account) {
        emailText = emailText.replaceAll("\\{username\\}", account.getUserName());
        emailText = emailText.replaceAll("\\{currentDate\\}", LocalDate.now().toString());

        return emailText;
    }

    public void sendMessageAboutTransfer(Account myAccount, int transferAmount) {
        Email email = emailRepository.getEmailByName("Transfer email");
        String to = myAccount.getEmail();
        String emailText = replaceVariables(email.getEmailBody(), myAccount);

        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setTo(to);
            messageHelper.setSubject(email.getEmailSubject());
            messageHelper.setText(emailText, true);
        };
        emailSender.send(messagePreparator);
    }



    public List<EmailDetails> getAllAllEmails() {
        return emailRepository.getAllEmailDetails();
    }

    public void updateEmail(EmailForm emailForm) {

        Optional<Email> emailOptional = emailRepository.findById(emailForm.getId());

        if (emailOptional.isPresent()) {
            Email email = emailOptional.get();
            email.setEmailBody(emailForm.getEmailBody());

            System.out.println("email: " + email.getEmailBody());

            emailRepository.save(email);
        }
    }

}

