/*
 * Copyright © Progmasters (QTC Kft.), 2018.
 * All rights reserved. No part or the whole of this Teaching Material (TM) may be reproduced, copied, distributed,
 * publicly performed, disseminated to the public, adapted or transmitted in any form or by any means, including
 * photocopying, recording, or other electronic or mechanical methods, without the prior written permission of QTC Kft.
 * This TM may only be used for the purposes of teaching exclusively by QTC Kft. and studying exclusively by QTC Kft.’s
 * students and for no other purposes by any parties other than QTC Kft.
 * This TM shall be kept confidential and shall not be made public or made available or disclosed to any unauthorized person.
 * Any dispute or claim arising out of the breach of these provisions shall be governed by and construed in accordance with the laws of Hungary.
 */

package com.progmasters.fundraiser.dto.out;

import com.progmasters.fundraiser.domain.Account;
import com.progmasters.fundraiser.domain.Email;

import java.util.ArrayList;
import java.util.List;

public class EmailDetails {
    private Long id;
    private String emailBody;
    private String allowedParameter;
    private String emailSubject;
    private String templateName;

    public EmailDetails() {
    }

    public EmailDetails(Email email) {
        this.id = email.getId();
        this.emailBody = email.getEmailBody();
        this.allowedParameter = email.getAllowedParameter();
        this.emailSubject = email.getEmailSubject();
        this.templateName = email.getTemplateName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public String getAllowedParameter() {
        return allowedParameter;
    }

    public void setAllowedParameter(String allowedParameter) {
        this.allowedParameter = allowedParameter;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }
}
