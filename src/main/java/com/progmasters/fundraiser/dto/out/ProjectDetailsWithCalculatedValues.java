package com.progmasters.fundraiser.dto.out;

import com.progmasters.fundraiser.domain.Project;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class ProjectDetailsWithCalculatedValues {

    private Long id;

    private String name;

    private String description;

    private Integer goal;

    private Integer funds;

    private CategoryDetails category;

    private String deadline;

    private String imageUrl;

    private String remainingDays;

    private String account;

    private String currency;


    public ProjectDetailsWithCalculatedValues() {
    }

    public ProjectDetailsWithCalculatedValues(Project project) {
        this.name = project.getName();
        this.description = project.getDescription();
        this.goal = project.getGoalFunds();
        this.category = new CategoryDetails(project.getCategory());
        this.deadline = project.getDeadline().toString();
        this.imageUrl = project.getImageUrl();
        if (project.getImageUrl() != null) {
            this.imageUrl = project.getImageUrl();
        }
        this.remainingDays = calculateDays(project.getDeadline());
        this.funds = project.getFunds();
        this.account = project.getAccount().getUserName();
    }

    private String calculateDays(LocalDate deadline) {

        String remainingDaysString = "";

        final LocalDate today = LocalDate.now();
        int numberOfDays = (int)today.until(deadline, ChronoUnit.DAYS);

        if (numberOfDays > 1) {
            remainingDaysString = numberOfDays + " days left";
        } else if (numberOfDays == 1) {
            remainingDaysString = "1 day left";
        } else {
            remainingDaysString = "The raising is over";
        }

        return remainingDaysString;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getGoal() {
        return goal;
    }

    public void setGoal(Integer goal) {
        this.goal = goal;
    }

    public CategoryDetails getCategory() {
        return category;
    }

    public void setCategory(CategoryDetails category) {
        this.category = category;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getFunds() {
        return funds;
    }

    public void setFunds(Integer funds) {
        this.funds = funds;
    }

    public String getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(String remainingDays) {
        this.remainingDays = remainingDays;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
