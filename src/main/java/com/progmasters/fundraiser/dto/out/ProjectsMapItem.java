package com.progmasters.fundraiser.dto.out;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectsMapItem {

    private Map<String, Long> projectMap;

    public ProjectsMapItem() {
    }

    public ProjectsMapItem(List<ProjectListItem> projectListItems) {
        this.projectMap = createMapFromListItem(projectListItems);
    }

    private Map<String,Long> createMapFromListItem(List<ProjectListItem> projectListItems) {
        Map<String,Long> resultMap = new HashMap<>();
        for (ProjectListItem item: projectListItems) {
            resultMap.put(item.getName(), item.getId());
        }
        return resultMap;
    }

    public Map<String, Long> getProjectMap() {
        return projectMap;
    }
}
