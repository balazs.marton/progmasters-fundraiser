package com.progmasters.fundraiser.dto.out;


public class TransferGroupByDayListItem {
    private String date;
    private Long oneDayFunds;

    public TransferGroupByDayListItem() {
    }

    public TransferGroupByDayListItem(String date, long oneDayFunds) {
        this.date = date;
        this.oneDayFunds = oneDayFunds;
    }

    public String getDate() {
        return date;
    }

    public Long getOneDayFunds() {
        return oneDayFunds;
    }
}
