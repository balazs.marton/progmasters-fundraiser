/*
 * Copyright © Progmasters (QTC Kft.), 2018.
 * All rights reserved. No part or the whole of this Teaching Material (TM) may be reproduced, copied, distributed,
 * publicly performed, disseminated to the public, adapted or transmitted in any form or by any means, including
 * photocopying, recording, or other electronic or mechanical methods, without the prior written permission of QTC Kft.
 * This TM may only be used for the purposes of teaching exclusively by QTC Kft. and studying exclusively by QTC Kft.’s
 * students and for no other purposes by any parties other than QTC Kft.
 * This TM shall be kept confidential and shall not be made public or made available or disclosed to any unauthorized person.
 * Any dispute or claim arising out of the breach of these provisions shall be governed by and construed in accordance with the laws of Hungary.
 */

package com.progmasters.fundraiser.dto.out;

import com.progmasters.fundraiser.domain.Account;

import java.util.ArrayList;
import java.util.List;

public class AccountDetails implements Comparable<AccountDetails> {

    private Long id;
    private String userName;
    private String email;
    private String goal;
    private Integer balance;
    private Integer funds;
    private String currency;
    private List<ProjectListItem> projects = new ArrayList<>();

    public AccountDetails() {
    }

    public AccountDetails(Account account) {
        this.id = account.getId();
        this.userName = account.getUserName();
        this.goal = account.getConfirmPassword();
        this.balance = account.getCurrency().exchangeFromUsd(account.getBalance());
        this.funds = account.getFunds();
        this.email = account.getEmail();
        this.currency = account.getCurrency().getName();
    }

    public AccountDetails(Account account, List<ProjectListItem> myAccountProjects) {
        this(account);
        this.projects = myAccountProjects;
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getGoal() {
        return goal;
    }

    public Integer getBalance() {
        return balance;
    }

    public Integer getFunds() {
        return funds;
    }




    @Override
    public int compareTo(AccountDetails otherAccountDetails) {
        return otherAccountDetails.funds.compareTo(this.funds);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<ProjectListItem> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectListItem> projects) {
        this.projects = projects;
    }
}
