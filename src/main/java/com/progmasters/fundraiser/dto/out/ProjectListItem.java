package com.progmasters.fundraiser.dto.out;

import com.progmasters.fundraiser.domain.Project;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class ProjectListItem {

    private Long id;

    private String name;

    private String description;

    private String imageUrl;

    private String remainingDays;

    private Integer funds;

    private Integer goalFunds;

    private String currency;

    private String createDate;

    public ProjectListItem() {
    }

    public ProjectListItem(Project project) {
        this.id = project.getId();
        this.name = project.getName();
        this.description = project.getDescription();
        if (project.getImageUrl() != null) {
            this.imageUrl = project.getImageUrl();
        }
        this.remainingDays = calculateDays(project.getDeadline());
        this.funds = project.getFunds();
        this.goalFunds = project.getGoalFunds();
        this.createDate = project.getCreateDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    private String calculateDays(LocalDate deadline) {

        String remainingDaysString = "";

        final LocalDate today = LocalDate.now();
        int numberOfDays = (int)today.until(deadline, ChronoUnit.DAYS);

        if (numberOfDays > 1) {
            remainingDaysString = "Remaining: " + numberOfDays + " days";
        } else if (numberOfDays == 1) {
            remainingDaysString = "Remaining: 1 day";
        } else {
            remainingDaysString = "The raising is over";
        }

        return remainingDaysString;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(String remainingDays) {
        this.remainingDays = remainingDays;
    }

    public Integer getFunds() {
        return funds;
    }

    public void setFunds(Integer funds) {
        this.funds = funds;
    }

    public Integer getGoalFunds() {
        return goalFunds;
    }

    public void setGoalFunds(Integer goalFunds) {
        this.goalFunds = goalFunds;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
