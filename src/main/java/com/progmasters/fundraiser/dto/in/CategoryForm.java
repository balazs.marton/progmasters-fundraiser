package com.progmasters.fundraiser.dto.in;

import com.progmasters.fundraiser.domain.Category;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CategoryForm {

    private Long id;

    @NotNull
    @Size(min = 1)
    private String name;

    public CategoryForm() { }

    public CategoryForm(Category category) {
        this.id = category.getId();
        this.name = category.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
