package com.progmasters.fundraiser.dto.in;

import com.progmasters.fundraiser.domain.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EmailForm {

    private Long id;

    private String emailBody;

    public EmailForm() { }

    public EmailForm(Email email) {
        this.id = email.getId();
        this.emailBody = email.getEmailBody();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }
}
