package com.progmasters.fundraiser.dto.in;

public class TransferCreationCommand {

    private Long projectId;
    private Integer amount;

    public TransferCreationCommand() {
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long productId) {
        this.projectId = productId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }


}
