package com.progmasters.fundraiser.service;

import com.progmasters.fundraiser.domain.*;
import com.progmasters.fundraiser.dto.in.ProjectForm;
import com.progmasters.fundraiser.repository.AccountRepository;
import com.progmasters.fundraiser.repository.CategoryRepository;
import com.progmasters.fundraiser.repository.ProjectRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProjectServiceTest {

    private ProjectService projectService;

    private ProjectRepository projectRepositoryMock;

    private AccountRepository accountRepositoryMock;

    private CategoryRepository categoryRepositoryMock;


    @Before
    public void setUp() {
        this.projectRepositoryMock = mock(ProjectRepository.class);
        this.accountRepositoryMock = mock(AccountRepository.class);
        this.categoryRepositoryMock = mock(CategoryRepository.class);
        this.projectService = new ProjectService(projectRepositoryMock, accountRepositoryMock, categoryRepositoryMock);
    }

    @Test
    public void testCreateProject() {
        Account account = getNewAccount();

        Category category = new Category();
        category.setId(1L);

        Project project = getNewProject();
        project.setAccount(account);
        project.setCategory(category);

        when(accountRepositoryMock.findByUserName(any(String.class))).thenReturn(account);
        when(categoryRepositoryMock.findById(1L)).thenReturn(Optional.of(category));
        when(projectRepositoryMock.save(any(Project.class))).thenAnswer(returnsFirstArg());

        Project savedProject = projectService.create(getNewProjectForm(),
                new UsernamePasswordAuthenticationToken(null, null));

        assertEquals(project.getId(), savedProject.getId());
        assertEquals(project.getName(), savedProject.getName());
        assertEquals(project.getAccount().getId(), savedProject.getAccount().getId());
        assertEquals(project.getCategory().getId(), savedProject.getCategory().getId());
        assertEquals(project.getDeadline(), savedProject.getDeadline());
        assertEquals(project.getGoalFunds(), savedProject.getGoalFunds());
        assertEquals(project.getDescription(), savedProject.getDescription());
        assertEquals(project.getImageUrl(), savedProject.getImageUrl());
    }

    @Test
    public void testUpdateProject() {
        Project project = getNewProject();
        when(projectRepositoryMock.findById(any(Long.class))).thenReturn(Optional.of(project));

        ProjectForm projectForm = getNewProjectForm();
        projectForm.setGoalFunds(50);
        projectForm.setName("New Project Name");

        Account account = getNewAccount();
        Category category = getNewCategory();

        when(accountRepositoryMock.findById(any())).thenReturn(Optional.of(account));
        when(categoryRepositoryMock.findById(any())).thenReturn(Optional.of(category));
        when(projectRepositoryMock.save(any(Project.class))).thenAnswer(returnsFirstArg());

        Project newProject = projectService.update(projectForm);

        assertEquals(project.getId(), newProject.getId());
        assertEquals(project.getName(), newProject.getName());
        assertEquals(project.getAccount().getId(), newProject.getAccount().getId());
        assertEquals(project.getCategory().getId(), newProject.getCategory().getId());
        assertEquals(project.getDeadline(), newProject.getDeadline());
        assertEquals(project.getGoalFunds(), newProject.getGoalFunds());
        assertEquals(project.getDescription(), newProject.getDescription());
        assertEquals(project.getImageUrl(), newProject.getImageUrl());
    }

    private Category getNewCategory() {
        Category category = new Category();
        category.setId(1L);
        category.setName("Technology");

        return category;
    }

    private Account getNewAccount() {
        Account account = new Account();
        account.setId(1L);
        account.setUserName("Krissz");
        account.setFunds(10000);
        account.setBalance(5000);
        account.setCurrency(Currency.EUR);
        account.setIpAddress("192.168.1.2");
        account.setRole(Role.ROLE_USER);
        account.setEmail("bpkriszti68@hotmail.com");

        return account;
    }

    private ProjectForm getNewProjectForm() {
        ProjectForm projectForm = new ProjectForm();
        projectForm.setId(1L);
        projectForm.setName("Project-X!");
        projectForm.setUserId(1L);
        projectForm.setCategoryId(1L);
        projectForm.setDeadline(LocalDate.of(2018, 10, 8));
        projectForm.setGoalFunds(10000);
        projectForm.setDescription("This is my description!");
        projectForm.setImageUrl("https://i.ytimg.com/vi/CvIvwq6rz10/maxresdefault.jpg");

        return projectForm;
    }

    private Project getNewProject() {
        return new Project(getNewProjectForm());
    }
}
