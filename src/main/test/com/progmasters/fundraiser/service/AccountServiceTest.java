package com.progmasters.fundraiser.service;

import com.progmasters.fundraiser.domain.Account;
import com.progmasters.fundraiser.dto.in.AccountRegistrationCommand;
import com.progmasters.fundraiser.repository.AccountRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class AccountServiceTest {

    private AccountService accountService;

    private AccountRepository accountRepositoryMock;

    @Before
    public void setUp() {
        this.accountRepositoryMock = mock(AccountRepository.class);
        this.accountService = new AccountService(this.accountRepositoryMock,
                mock(PasswordEncoder.class),
                mock(ProjectService.class),
                mock(EmailServiceImpl.class));
    }

    @Test
    public void testAccountRegistration() {
        AccountRegistrationCommand accountRegistrationCommand = new AccountRegistrationCommand();
        accountRegistrationCommand.setUserName("Krissz");
        accountRegistrationCommand.setEmail("bpkriszti68@hotmail.com");
        accountRegistrationCommand.setPassword("nemtomlol");
        accountRegistrationCommand.setConfirmPassword("nemtomlol");
        accountRegistrationCommand.setCurrency("USD");

        Account account = new Account(accountRegistrationCommand);

        when(accountRepositoryMock.save(any(Account.class))).thenReturn(account);

        Account savedAccount = accountService.registerAccount(accountRegistrationCommand, new MockHttpServletRequest());

        assertEquals(account.getUserName(), savedAccount.getUserName());
        assertEquals(account.getEmail(), savedAccount.getEmail());
        assertEquals(account.getCurrency(), savedAccount.getCurrency());

        verify(accountRepositoryMock, times(1)).save(any(Account.class));
        verifyNoMoreInteractions(accountRepositoryMock);
    }
}
