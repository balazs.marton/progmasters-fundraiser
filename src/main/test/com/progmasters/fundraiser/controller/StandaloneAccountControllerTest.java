package com.progmasters.fundraiser.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.progmasters.fundraiser.controller.entity.AccountEntityController;
import com.progmasters.fundraiser.domain.Account;
import com.progmasters.fundraiser.domain.Currency;
import com.progmasters.fundraiser.domain.Role;
import com.progmasters.fundraiser.dto.in.AccountRegistrationCommand;
import com.progmasters.fundraiser.exception.GlobalExceptionHandler;
import com.progmasters.fundraiser.service.AccountService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.servlet.http.HttpServletRequest;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class StandaloneAccountControllerTest {

    private MockMvc mockMvc;

    @Mock
    private AccountService accountServiceMock;

    @InjectMocks
    private AccountEntityController accountEntityController;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(accountEntityController)
                .setControllerAdvice(new GlobalExceptionHandler(messageSource()))
                .build();
    }

    @After
    public void validate() {
        validateMockitoUsage();
    }

    @Test
    public void testRegisterAccountValid() throws Exception {
        AccountRegistrationCommand reg = new AccountRegistrationCommand();
        reg.setUserName("Krissz");
        reg.setEmail("bpkriszti68hotmail.com");
        reg.setPassword("mypassword");
        reg.setConfirmPassword("mypassword");
        reg.setCurrency(Currency.EUR.getName());

        this.mockMvc.perform(post("/api/account/entity/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(reg)))
                .andExpect(status().is(201));
//                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(jsonPath("$.userName", is("Krissz")))
//                .andExpect(jsonPath("$.email", is("bpkriszti68@hotmail.com")))
//                .andExpect(jsonPath("$.balance", is(5000)))
//                .andExpect(jsonPath("$.funds", is(0)));

        verify(accountServiceMock, times(1)).registerAccount(any(AccountRegistrationCommand.class), any());
        verifyZeroInteractions(accountServiceMock);
    }

    private Account getNewAccount() {
        Account account = new Account();
        account.setId(1L);
        account.setUserName("Krissz");
        account.setFunds(10000);
        account.setBalance(5000);
        account.setCurrency(Currency.EUR);
        account.setIpAddress("192.168.1.2");
        account.setRole(Role.ROLE_USER);
        account.setEmail("bpkriszti68@hotmail.com");

        return account;
    }

    private MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();

        messageSource.setBasename("messages");
        messageSource.setUseCodeAsDefaultMessage(true);

        return messageSource;
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
