package com.progmasters.fundraiser.controller;

import com.progmasters.fundraiser.config.SpringWebConfig;
import com.progmasters.fundraiser.domain.Category;
import com.progmasters.fundraiser.dto.out.CategoryDetails;
import com.progmasters.fundraiser.service.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {SpringWebConfig.class, AccountControllerTest.TestConfiguration.class})
public class AccountControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    AccountService accountServiceMock;

    @Before
    public void setup() {
        Mockito.reset(accountServiceMock);

        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testGetAllCategories() throws Exception {
        Category category1 = new Category();
        category1.setId(1L);
        category1.setName("Technology");

        Category category2 = new Category();
        category2.setId(2L);
        category2.setName("Biology");

        Category category3 = new Category();
        category3.setId(3L);
        category3.setName("Nature");

        List<CategoryDetails> categories = Arrays.asList(category1, category2, category3).stream()
                .map(CategoryDetails::new).collect(Collectors.toList());


        this.mockMvc.perform(get("/api/orcs"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(3)));

        verify(accountServiceMock).getAllAccounts();
    }

    @Configuration
    static class TestConfiguration {

        @Bean
        public AccountService accountService() {
            return Mockito.mock(AccountService.class);
        }
    }
}
