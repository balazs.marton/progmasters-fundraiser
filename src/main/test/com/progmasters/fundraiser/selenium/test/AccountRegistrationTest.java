package com.progmasters.fundraiser.selenium.test;

import com.progmasters.fundraiser.selenium.environment.EnvironmentManager;
import com.progmasters.fundraiser.selenium.environment.RunEnvironment;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class AccountRegistrationTest {

    @Before
    public void startBrowser() {
        EnvironmentManager.initWebDriver();
    }

    @Test
    public void AccountRegistrationTest() {
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.get("http://localhost:3000/registration");

        driver.findElement(By.cssSelector("input[name='userName']")).sendKeys("Krissz");
        driver.findElement(By.cssSelector("input[name='email']")).sendKeys("bpkriszti68@hotmail.com");
        driver.findElement(By.cssSelector("input[name='password']")).sendKeys("mypassword");
        driver.findElement(By.cssSelector("input[name='confirmPassword']")).sendKeys("mypassword");
        new Select(driver.findElement(By.cssSelector("select[name='currency']"))).selectByVisibleText("USD");
    }

    @After
    public void tearDown() {
        EnvironmentManager.shutDownDriver();
    }
}
