import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom";
import Styles from '../../common/Styles'
import PermissionButton from '../../PermissionComponents/PermissionButtons/PermissionButton';

function HomePage(props) {
    const { classes } = props;

    return (
        <React.Fragment>
            <CssBaseline />
            <main>
                <div className={'heroImageMainPage'}>
                    <div className={classes.heroContent}>
                        <Typography variant="display3" align="center" color="textPrimary" gutterBottom>
                            Boost your project
                        </Typography>
                        <Typography variant="title" align="center" color="textSecondary" paragraph>
                            Our mission is to help bring creative projects to life.
                        </Typography>
                        <div className={classes.heroButtons}>
                            <Grid container spacing={16} justify="center">
                                <Grid item>
                                    <Button variant="contained" color="primary" component={Link} to={"/projects"}>
                                        Take a look of the projects
                                    </Button>
                                </Grid>
                                <Grid item>
                                    <Fragment>
                                        <PermissionButton
                                            variant="contained"
                                            color="secondary"
                                            component={Link}
                                            to={"/create-project"}
                                            role={["ROLE_USER"]}
                                            >
                                            Create a project
                                        </PermissionButton>
                                        <PermissionButton
                                            variant="contained"
                                            color="secondary"
                                            component={Link}
                                            to={"/registration"}
                                            role={["ROLE_ANONYMOUS"]}
                                            >
                                            Register to create a project
                                        </PermissionButton>
                                    </Fragment>
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                </div>
                {/* End hero unit */}
                <div className={classNames(classes.layout, classes.cardGrid)}>
                    <Grid container spacing={40}>
                        <Grid item sm={6} md={4}>
                            <Card className={classes.card}>
                                <CardMedia
                                    className={classes.cardMedia}
                                    image="https://res.cloudinary.com/dup84qpgn/image/upload/v1538071632/iksoqig2ncvvrlexhpyf.webp" // eslint-disable-line max-len
                                    title="HELIO: Solar Light & Powerbank"
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="headline" component="h2">
                                        HELIO: Solar Light & Powerbank
                                    </Typography>
                                    <Typography>
                                        HELIO is an ultra-efficient solar lantern
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item sm={6} md={4}>
                            <Card className={classes.card}>
                                <CardMedia
                                    className={classes.cardMedia}
                                    image="https://res.cloudinary.com/dup84qpgn/image/upload/v1538072129/khs6af7vimkwvpizgz97.png" // eslint-disable-line max-len
                                    title="The Perfect Running Shoes"
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="headline" component="h2">
                                        The Perfect Running Shoes
                                    </Typography>
                                    <Typography>
                                        The comfy of your foot and muscles supported by the Power and Flexibility of the
                                        Tracks.
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item sm={6} md={4}>
                            <Card className={classes.card}>
                                <CardMedia
                                    className={classes.cardMedia}
                                    image="https://res.cloudinary.com/dup84qpgn/image/upload/v1538072734/cf2ggixufphiasmvj5cq.webp" // eslint-disable-line max-len
                                    title="iJoou: Smart Moxibustion Pain Relief Device"
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="headline" component="h2">
                                        iJoou: Smart Moxibustion Pain Relief Device
                                    </Typography>
                                    <Typography>
                                        A powerful penetrating herbal remedy for natural pain relief. Hands-Free,
                                        Portable, and App Control.
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item sm={6} md={4}>
                            <Card className={classes.card}>
                                <CardMedia
                                    className={classes.cardMedia}
                                    image="https://res.cloudinary.com/dup84qpgn/image/upload/v1538072734/zzhay9adockyhcb3qhca.jpg" // eslint-disable-line max-len
                                    title="Oclean Air: World's Most Compact Smart Toothbrush"
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="headline" component="h2">
                                        Oclean Air: World's Most Compact Smart Toothbrush
                                    </Typography>
                                    <Typography>
                                        Get Dental-Cleaned Teeth and Customizable Brushing Plans For a White Smile
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item sm={6} md={4}>
                            <Card className={classes.card}>
                                <CardMedia
                                    className={classes.cardMedia}
                                    image="https://res.cloudinary.com/dup84qpgn/image/upload/v1538071632/prgycbm67tgv2lvj4bup.webp" // eslint-disable-line max-len
                                    title="GoSun Go: Boil Water & Cook Meals with Solar Power"
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="headline" component="h2">
                                        GoSun Go: Boil Water & Cook Meals with Solar Power
                                    </Typography>
                                    <Typography>
                                        This 2LB (0.9kg) sun oven is versatile and durable enough to take anywhere!
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item sm={6} md={4}>
                            <Card className={classes.card}>
                                <CardMedia
                                    className={classes.cardMedia}
                                    image="https://res.cloudinary.com/dup84qpgn/image/upload/v1538070776/peslywfpuxbnepkxkqvf.png" // eslint-disable-line max-len
                                    title="Qbi Toy- Magnetic Modular Block Toys"
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="headline" component="h2">
                                        Qbi Toy- Magnetic Modular Block Toys
                                    </Typography>
                                    <Typography>
                                        A simple, versatile, and modular block toys that let parents and children enjoy
                                        the fun together.
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </div>
            </main>

        </React.Fragment>
    );
}

HomePage.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(Styles.homePageStyle)(HomePage);