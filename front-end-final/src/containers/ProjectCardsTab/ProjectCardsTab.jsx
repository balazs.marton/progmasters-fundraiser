import React, {Component, Fragment} from 'react';
import CssBaseline from "@material-ui/core/CssBaseline";
import PropTypes from "prop-types";
import {withStyles} from "@material-ui/core/styles";
import Styles from '../../common/Styles';
import Grid from "@material-ui/core/Grid";
import ProjectCardWithGraph from "../ProjectCardWithGraph/ProjectCardWithGraph";


class ProjectCardsTab extends Component {
    state = {
        open: false,
        rows: {}
    };

    handleClickOpen = (projectId) => {
        let updateRows = this.state.rows;
        updateRows[projectId] = true;
        this.setState(updateRows);
        console.log(this.state.rows)
    };

    handleClose = (projectId) => {
        let updateRows = this.state.rows;
        updateRows[projectId] = false;
        this.setState(updateRows);
    };

    render() {
        return (
            <Fragment>
                <CssBaseline/>

                {this.props.projects.map(project => {

                    return (
                        <Grid container spacing={40} key={project.id}>
                            <ProjectCardWithGraph
                                project={project}
                            />
                        </Grid>
                    );
                })}

            </Fragment>
        );
    }
}

ProjectCardsTab.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(Styles.projectTableStyle)(ProjectCardsTab);