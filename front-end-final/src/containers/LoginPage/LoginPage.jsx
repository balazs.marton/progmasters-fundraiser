import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import React, { Component, Fragment } from 'react';
import axios from 'axios';
import Styles from '../../common/Styles'
import Urls from '../../common/Urls'


class LoginPage extends Component {
    constructor(props) {
        super(props);
        document.title = 'Login form';
    }

    state = {
        userName: {
            value: '',
            isValid: true,
            message: ''
        },
        password: {
            value: '',
            isValid: true,
            message: ''
        }
    };

    placeholder = {
        name: "Username",
        password: "Password"
    }

    inputChangeHandler = (event) => {
        const target = event.target;
        const updatedFormElement = {
            ...this.state[target.name]
        };

        let value;
        if (target.type === 'checkbox') {
            value = target.checked
                ? updatedFormElement.value.concat(target.value)
                : updatedFormElement.value.filter(val => val !== target.value);
        } else {
            value = target.value;
        }

        updatedFormElement.value = value;

        this.setState({ [target.name]: updatedFormElement });
    };

    validationHandler = (error) => {
        const updatedState = {
            ...this.state
        };

        for (let field in this.state) {
            const updatedFormElement = {
                ...updatedState[field]
            };
            updatedFormElement.isValid = true;
            updatedFormElement.message = '';
            updatedState[field] = updatedFormElement;
        }

        for (let fieldError of error.fieldErrors) {
            const updatedFormElement = {
                ...updatedState[fieldError.field]
            };
            updatedFormElement.isValid = false;
            updatedFormElement.message = fieldError.message;
            updatedState[fieldError.field] = updatedFormElement;
        }

        this.setState(updatedState);
    }

    postDataHandler = (event) => {
        event.preventDefault();

        const formData = {};
        for (let formElementIdentifier in this.state) {
            formData[formElementIdentifier] = this.state[formElementIdentifier].value;
        }

        axios.get(Urls.account.list +'/login', {
            auth: {
                username: this.state.userName.value,
                password: this.state.password.value
            }
        })
            .then(response => {
                console.log("Logging response:", response);
                localStorage.setItem('user', JSON.stringify(response.data));
                const user = JSON.parse(localStorage.getItem('user'));
                console.log(user.id);
                console.log(user.name);
                console.log(user.role);

                this.props.history.push('/');  //redirect
            })
            .catch(error => {
                console.log(error.response);

                const errors = {
                    fieldErrors: [
                        {
                            field: 'userName',
                            message: 'Invalid username or password'
                        }
                    ]
                }
                this.validationHandler(errors);
            });
    }

    render() {
        console.log(this.props)
        const { classes } = this.props;

        return (
            <Fragment>
                <CssBaseline />
                <main className={classes.layout}>
                    <Paper className={classes.paper}>
                        <img src="/logo.svg" alt="Fundraiser" width="200" />
                        <Typography variant="headline">Sign in</Typography>
                        <form className={classes.form} onSubmit={this.postDataHandler}>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="name">User Name:</InputLabel>
                                <Input
                                    name="userName"
                                    type="text"
                                    id="name"
                                    autoFocus
                                    onChange={this.inputChangeHandler}
                                    value={this.state.userName.value}
                                    placeholder={this.placeholder.name}
                                />
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="name">Password:</InputLabel>
                                <Input
                                    name="password"
                                    type="password"
                                    id="name"
                                    onChange={this.inputChangeHandler}
                                    value={this.state.password.value}
                                />
                            </FormControl>
                            <Button
                                type="submit"
                                fullWidth
                                variant="raised"
                                color="primary"
                                className={classes.submit}
                            >
                                Sign in
                        </Button>
                        </form>
                    </Paper>
                </main>
            </Fragment>
        )
    }
}

LoginPage.propTypes ={
    classes: PropTypes.object.isRequired,
}

export default withStyles(Styles.loginStyle)(LoginPage);