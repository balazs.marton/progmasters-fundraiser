import React, {Component, Fragment} from 'react';
import axios from "axios";

import PropTypes from "prop-types";
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import classNames from "classnames";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
// import FormHelperText from '@material-ui/core/FormHelperText';
import ReactDropzone from "react-dropzone";
import Styles from '../../common/Styles'
import Urls from '../../common/Urls'


class ProjectForm extends Component {

    constructor(props) {
        super(props);
        document.title = 'Project form';
    }

    state = {
        categories: [],
        files: [],
        formData: {
            id: {
                value: null,
                isValid: true,
                errorMessage: ""
            },
            name: {
                value: "",
                isValid: true,
                errorMessage: ""
            },
            description: {
                value: "",
                isValid: true,
                errorMessage: ""
            },
            goal: {
                value: "",
                isValid: true,
                errorMessage: ""
            },
            category: {
                id: "",
                value: "",
                isValid: true,
                errorMessage: ""
            },
            deadline: {
                value: "",
                isValid: true,
                errorMessage: ""
            },
            imageUrl: {
                value: "",
                isValid: true,
                errorMessage: ""
            },
        }
    };

    componentDidMount() {
        axios.get(Urls.category.list)
            .then(response => {
                this.setState({
                    categories: response.data
                })
            })
            .catch(error => {
                console.log("Logging errors in componentDidMount:", error);
            });

        if (this.props.match.params.id) {
            axios.get(Urls.project.list + "/" + this.props.match.params.id)
                .then(response => {
                    console.log("Logging response.data in componentDidMount:", response.data);
                    this.setState({
                        formData: {
                            id: {
                                value: this.props.match.params.id,
                                isValid: true,
                                errorMessage: ""
                            },
                            name: {
                                value: response.data.name,
                                isValid: true,
                                errorMessage: ""
                            },
                            description: {
                                value: response.data.description,
                                isValid: true,
                                errorMessage: ""
                            },
                            goal: {
                                value: response.data.goal,
                                isValid: true,
                                errorMessage: ""
                            },
                            category: {
                                value: response.data.category.id,
                                isValid: true,
                                errorMessage: ""
                            },
                            deadline: {
                                value: response.data.deadline,
                                isValid: true,
                                errorMessage: ""
                            },
                            imageUrl: {
                                value: response.data.imageUrl,
                                isValid: true,
                                errorMessage: ""
                            },
                        }
                    })
                })
        }
    }

    inputChangeHandler = (event) => {
        const value = event.target.value;
        const fieldName = event.target.name;
        const updatedFormData = {...this.state.formData};

        updatedFormData[fieldName] = {
            value: value,
            isValid: true,
            errorMessage: null
        };

        this.setState({formData: updatedFormData});
    };

    validationHandler = error => {
        const responseData = error.response.data;
        const updatedFormData = {...this.state.formData};
        if (responseData !== null) {
            if (responseData.globalError) {
                this.setState({globalMessage: responseData.globalError});
            }
            if (responseData.fieldErrors) {
                for (let fe of responseData.fieldErrors) {
                    updatedFormData[fe.field] = {
                        value: this.state.formData[fe.field].value,
                        isValid: false,
                        errorMessage: fe.message
                    }
                }
            }
            this.setState({formData: updatedFormData});
        } else {
            this.setState({globalMessage: error});
        }
    };

    formSubmit = event => {
        event.preventDefault();

        let newImageUrl;

        if (this.state.files && this.state.files.length > 0) {
            // Fill file variable with the file content
            this.file = this.state.files[0];

            let body = new FormData();
            // Add file content to prepare the request
            body.append("file", this.file);

            axios.post(Urls.project.entity + "/image", body)
                .then(resp => {
                    newImageUrl = resp.data[0];
                    this.sendFormDatas(newImageUrl);
                })
        } else {
            this.sendFormDatas();
        }
    }


    //
    // sendFormDatas = (datas) => {
    //     let newPictureUrl = datas ? datas : this.state.formData.imageUrl.value;
    //
    //     const data = {
    //         name: this.state.formData.name.value,
    //         address: this.state.formData.address.value,
    //         imageUrl: newPictureUrl
    //     };
    //
    //     axios.post(this.HOTEL_FORM_URL, data)
    //         .then(() => {
    //             this.props.history.push("/");
    //         })
    //         .catch(error => {
    //             console.warn("error.response: ", error === null ? error.response : error);
    //             this.validationHandler(error);
    //         });
    // };

    sendFormDatas = (datas) => {

        let newPictureUrl = datas ? datas : this.state.formData.imageUrl.value;

        const data = {
            id: this.state.formData.id.value,
            name: this.state.formData.name.value,
            description: this.state.formData.description.value,
            goalFunds: this.state.formData.goal.value,
            deadline: this.state.formData.deadline.value,
            imageUrl: newPictureUrl,
            userId: 1,
            categoryId: this.state.formData.category.value
        };

        console.log(data);

        if (data.id) {
            axios.put(Urls.project.entity + "/update-project", data)
                .then(() => {
                    this.props.history.push("/projects");
                })
                .catch(error => {
                    console.warn("error.response: ", error === null ? error.response : error);
                    console.log("error.response.data: ", error === null ? error : error);
                    this.validationHandler(error);
                });
        } else {
            axios.post(Urls.project.entity + "/create-project", data)
                .then(() => {
                    console.log("Posted");
                    this.props.history.push("/projects");
                })
                .catch(error => {
                    console.warn("error.response: ", error === null ? error.response : error);
                    console.log("error.response.data: ", error === null ? error : error);
                    this.validationHandler(error);
                });
        }
    };

    onPreviewDrop = (files) => {
        this.setState({
            files: this.state.files.concat(files),
        });
    };

    render() {

        const {classes} = this.props;
        const dropzoneStyle = {
            width: "100%",
            height: "150px",
            border: "2px dashed #d5d4d4",
            borderRadius: "7px",
            paddingBottom: '10px',
            marginTop: '20px',
            textAlign: 'center'
        };
        const dropzoneStyleActive = {
            background: "rgba(168,255,168,0.4)",
            border: "2px dashed green",
        };
        const dropzoneStyleRejected = {
            background: "rgba(255,0,0,0.35)",
            border: "2px dashed red",
        };

        const previewStyle = {
            width: '100%'
        };

        let imgUrl;
        if (this.state.formData.imageUrl.value !== null || this.state.formData.imageUrl.value.trim() !== '') {
            imgUrl = this.state.formData.imageUrl.value;
        }

        return (
            <Fragment>
                <CssBaseline/>
                <main>
                    <div className={classNames(classes.layout)}>
                        <Typography variant="headline" color="textPrimary" gutterBottom>
                            Project form
                        </Typography>
                        <Divider/>
                        <br/>

                        <Grid container spacing={40}>
                            <Grid item xs={12} md={4}>
                                {this.state.formData.imageUrl.value &&
                                <Fragment>
                                    <img className="img-fluid" src={imgUrl} alt="Projct"/>
                                    <br/>
                                    <br/>
                                </Fragment>
                                }
                                {(this.state.files.length < 1 && !this.state.formData.imageUrl.value)&&
                                <ReactDropzone
                                    accept="image/*"
                                    onDrop={this.onPreviewDrop}
                                    style={dropzoneStyle}
                                    activeStyle={dropzoneStyleActive}
                                    rejectStyle={dropzoneStyleRejected}
                                >
                                    <br/>
                                    Drop the new image here or click to open the file browser.
                                </ReactDropzone>
                                }
                                <Fragment>
                                    {this.state.files.map((file) => (
                                        <img
                                            alt="Preview"
                                            key={file.preview}
                                            src={file.preview}
                                            style={previewStyle}
                                        />
                                    ))}
                                </Fragment>

                            </Grid>
                            <Grid item xs={12} md={8}>
                                <form className={classes.form} onSubmit={this.formSubmit}>
                                    <FormControl margin="normal" required fullWidth>
                                        <InputLabel htmlFor="name">Project name:</InputLabel>
                                        <Input
                                            name="name"
                                            id="name"
                                            type="text"
                                            autoFocus
                                            onChange={this.inputChangeHandler}
                                            value={this.state.formData.name.value}
                                        />
                                        <small
                                            className="form-text error-block">{this.state.formData.name.errorMessage}</small>
                                    </FormControl>
                                    <FormControl margin="normal" required fullWidth>
                                        <InputLabel htmlFor="description">Description:</InputLabel>
                                        <Input
                                            name="description"
                                            id="description"
                                            type="text"
                                            onChange={this.inputChangeHandler}
                                            value={this.state.formData.description.value}
                                            multiline
                                            rows={6}
                                            rowsMax={16}
                                        />
                                        <small
                                            className="form-text error-block">{this.state.formData.description.errorMessage}</small>
                                    </FormControl>
                                    <FormControl margin="normal" fullWidth>
                                        <InputLabel htmlFor="goal">Goal:</InputLabel>
                                        <Input
                                            name="goal"
                                            type="number"
                                            onChange={this.inputChangeHandler}
                                            value={this.state.formData.goal.value}
                                        />
                                        <small
                                            className="form-text error-block">{this.state.formData.goal.errorMessage}</small>
                                    </FormControl>
                                    <FormControl className={classes.formControl}>
                                        <InputLabel htmlFor="category-simple">Category</InputLabel>
                                        <Select
                                            value={this.state.formData.category.value}
                                            onChange={this.inputChangeHandler}
                                            inputProps={{
                                                name: 'category',
                                                id: 'category-simple',
                                            }}
                                        >
                                            {this.state.categories.map((category, index) => {
                                                return (
                                                    <MenuItem
                                                        key={index}
                                                        value={category.id}>{category.name}</MenuItem>
                                                )
                                            })}
                                        </Select>
                                        <small
                                            className="form-text error-block">{this.state.formData.category.errorMessage}</small>
                                    </FormControl>
                                    <FormControl margin="normal" fullWidth>
                                        <TextField
                                            id="date"
                                            label="Deadline"
                                            name="deadline"
                                            type="date"
                                            defaultValue="2017-05-24"
                                            className={classes.textField}
                                            onChange={this.inputChangeHandler}
                                            value={this.state.formData.deadline.value}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                        <small
                                            className="form-text error-block">{this.state.formData.deadline.errorMessage}</small>
                                    </FormControl>
                                    <Button
                                        type="submit"
                                        variant="raised"
                                        color="primary"
                                        className={classes.submit}
                                    >
                                        Save project
                                    </Button>
                                </form>
                            </Grid>
                        </Grid>
                    </div>
                </main>
            </Fragment>
        );
    }
}

ProjectForm.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(Styles.projectFormStyle)(ProjectForm);