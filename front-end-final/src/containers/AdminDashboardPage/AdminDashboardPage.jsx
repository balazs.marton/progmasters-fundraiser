import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import SwipeableViews from 'react-swipeable-views';
import CssBaseline from '@material-ui/core/CssBaseline';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import EmailIcon from '@material-ui/icons/Email';

import axios from 'axios';
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import CreateCategoryPage from "../CreateCategoryPage/CreateCategoryPage";
import ProjectTable from "../ProjectTable/ProjectTable";
import Styles from '../../common/Styles';
import Urls from '../../common/Urls';
import TabContainer from "../../components/TabContainer";
import EmailTable from "../EmailTable/EmailTable";

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired,
};

class AdminDashboardPage extends Component {

    state = {
        projects: [],
        open: false,
        value: 0,
    };

    componentDidMount() {
        this.getProjects();
    }

    getProjects = () => {
        axios.get(Urls.project.list)
            .then(response => {
                console.log(response);
                this.setState({
                    projects: response.data
                })
            })
    };

    deleteHandler = (id) => {
        console.log("Logging delete button id:", id);
        axios.delete(Urls.project.entity + "/" + id)
            .then(response => {
                console.log("Logging response:", response);
                this.getProjects();
            })
            .catch(error => {
                console.log("Logging error:", error);
            })
    };

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleChange = (event, value) => {
        this.setState({value});
    };

    handleChangeIndex = index => {
        this.setState({value: index});
    };

    render() {
        const {classes} = this.props;

        return (
            <React.Fragment>
                <CssBaseline/>
                <main>
                    <div className={classNames(classes.layout)}>
                        <Typography variant="headline" color="textPrimary" gutterBottom>
                            Admin dashboard
                        </Typography>
                        <Divider/>
                        <br/>

                        <Tabs value={this.state.value} onChange={this.handleChange} scrollable scrollButtons="off">
                            <Tab icon={<i className="fas fa-list"></i>}  label="Projects"  />
                            <Tab icon={<EmailIcon />}   label="Emails" />
                            <Tab icon={<i className="fas fa-tags"></i>}   label="Categories" />
                        </Tabs>


                        <SwipeableViews
                            // axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                            index={this.state.value}
                            onChangeIndex={this.handleChangeIndex}
                        >
                            {this.state.value === 0 && <TabContainer>
                                <ProjectTable
                                    projects={this.state.projects}
                                    deleteHandler={this.deleteHandler}
                                />
                            </TabContainer>}
                            {this.state.value === 1 && <TabContainer>
                                <EmailTable/>
                            </TabContainer>}
                            {this.state.value === 2 && <TabContainer>

                                <Button color="primary" variant="contained" onClick={this.handleClickOpen}>
                                    Create category
                                </Button>
                                <Dialog
                                    open={this.state.open}
                                    onClose={this.handleClose}
                                    aria-labelledby="form-dialog-title"
                                >
                                    <DialogContent>
                                        <CreateCategoryPage
                                            onClick={this.handleClose}
                                        />
                                    </DialogContent>
                                </Dialog>
                            </TabContainer>}
                        </SwipeableViews>
                    </div>
                </main>

            </React.Fragment>
        )
    }
}

AdminDashboardPage.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(Styles.adminDashboardPageStyle)(AdminDashboardPage);