import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CssBaseline from "@material-ui/core/CssBaseline";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Typography from "@material-ui/core/Typography";
import {Link} from "react-router-dom";
import ProgressLine from "../../components/ProgressLine";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "../../../node_modules/@material-ui/icons/Edit";
import DeleteIcon from "../../../node_modules/@material-ui/icons/Delete";
import classNames from "classnames";
import PropTypes from "prop-types";
import {withStyles} from "@material-ui/core/styles";
import Styles from '../../common/Styles';


class ProjectTable extends Component {
    state = {
        open: false,
        rows : {}
    };

    handleClickOpen = (projectId) => {
        let updateRows = this.state.rows;
        updateRows[projectId] = true;
        this.setState(updateRows);
        console.log(this.state.rows)
    };

    handleClose = (projectId) => {
        let updateRows = this.state.rows;
        updateRows[projectId] = false;
        this.setState(updateRows);
    };

    render() {
        const {classes} = this.props;

        console.log('table datas: ', this.props);


        return (
            <React.Fragment>
                <CssBaseline/>
                <Table className={classNames(classes.table)}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Project name</TableCell>
                            <TableCell numeric>Raised funds / Goal</TableCell>
                            <TableCell numeric>Remaining</TableCell>
                            <TableCell numeric></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.projects.map(project => {

                            const editUrl = '/projects/' + project.id + '/edit';
                            const projectUrl = '/projects/' + project.id;

                            return (
                                <TableRow key={project.id}>
                                    <TableCell component="th" scope="row">
                                        <Typography className="projectTableLink" variant="body1" color="inherit">
                                            <Link to={projectUrl}> {project.name}</Link>
                                        </Typography>

                                    </TableCell>
                                    <TableCell numeric>
                                        {project.funds} / {project.goalFunds} {project.currency}<br/>
                                        <ProgressLine
                                            goal={project.goalFunds}
                                            current={project.funds}
                                        />
                                    </TableCell>
                                    <TableCell numeric>{project.remainingDays}</TableCell>
                                    <TableCell numeric>
                                        <IconButton color="inherit" component={Link} to={editUrl}>
                                            <EditIcon/>
                                        </IconButton>
                                        <IconButton color="inherit"  onClick={() => {
                                            this.handleClickOpen(project.id);
                                        }}>
                                            <DeleteIcon/>
                                        </IconButton>
                                        <Dialog
                                            open={this.state.rows[project.id]}
                                            onClose={() => {this.handleClose(project.id);}}
                                            aria-labelledby="alert-dialog-title"
                                            aria-describedby="alert-dialog-description"
                                        >
                                            <DialogTitle id="alert-dialog-title">{"Confirm delete"}</DialogTitle>
                                            <DialogContent>
                                                <DialogContentText id="alert-dialog-description">
                                                    Are you sure you want to delete this project?
                                                </DialogContentText>
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={() => {this.handleClose(project.id);}} color="primary">
                                                    Cancel
                                                </Button>
                                                <Button onClick={
                                                    () => {
                                                        this.props.deleteHandler(project.id);
                                                        this.handleClose(project.id);
                                                    }
                                                }
                                                        color="secondary" autoFocus>
                                                    Delete
                                                </Button>
                                            </DialogActions>
                                        </Dialog>

                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </React.Fragment>
        );
    }
}

ProjectTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(Styles.projectTableStyle)(ProjectTable);