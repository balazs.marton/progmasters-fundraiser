import React, { Component } from 'react';
import axios from 'axios';
import validator from 'validator';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import { withSnackbar } from 'notistack';
import Styles from '../../common/Styles'
import Urls from '../../common/Urls'

class RegistrationPage extends Component {

    constructor(props) {
        super(props);
        document.title = 'Registration Page';
    }

    state = {
        email: {
            value: '',
            isValid: true,
            message: ''

        },
        password: {
            value: '',
            isValid: true,
            message: ''
        },
        userName: {
            value: '',
            isValid: true,
            message: ''
        },
        confirmPassword: {
            value: '',
            isValid: true,
            message: ''
        },

        currency: {
            value: '',
            isValid: true,
            message: ''
        },

    };

    inputChangeHandler = (event) => {
        const updatedFromField = {};
        updatedFromField.value = event.target.value;
        updatedFromField.isValid = true;
        updatedFromField.message = '';

        if (event.target.name === 'userName') {
            if (updatedFromField.value.length < 4) {
                updatedFromField.isValid = false
                updatedFromField.message = 'Username must be between 4 and 30 characters'
            }
        }

        if (event.target.name === 'email') {

            if (!validator.isEmail(updatedFromField.value)) {
                updatedFromField.isValid = false
                updatedFromField.message = 'Invalid email format'
            }
        }

        if (event.target.name === 'password') {
            if (updatedFromField.value.length < 4) {
                updatedFromField.isValid = false
                updatedFromField.message = 'Size must be between 4 and 60 characters'
            }
        }

        if (event.target.name === 'confirmPassword') {
            if (!updatedFromField.value.length) {
                updatedFromField.isValid = false
                updatedFromField.message = 'Please confirm your password'
            }
        }

        this.setState({

            [event.target.name]: updatedFromField
        })
    }

    formSubmit = (event) => {
        event.preventDefault();

        const data = {};
        for (let formElementName in this.state) {
            data[formElementName] = this.state[formElementName].value;
        }
        const { onPresentSnackbar } = this.props;

        axios.post(Urls.account.entity + '/register', data)
            .then(() => onPresentSnackbar('success', 'Successfully registered'))
            .then(() => {
                this.props.history.push('/login');
            })
            .catch(error => {
                if (error.response) {
                    this.validationHandler(error.response.data);
                }
            })
    }

    validationHandler = (error) => {
        const statePatch = {};
        for (let fieldError of error.fieldErrors) {
            statePatch[fieldError.field] = {
                value: this.state[fieldError.field].value,
                isValid: false,
                message: fieldError.message
            }
        }
        this.setState(statePatch);
    }

    componentDidMount() {
        axios.get(Urls.account.list + '/myAccountDetails')
            .then(() => {
                this.props.history.push('/');
            })
            .catch(error => console.warn(error));
    }

    render() {
        const { classes } = this.props;
        return (
            <React.Fragment>
                <CssBaseline />
                <main className={classes.layout}>
                    <Paper className={classes.paper}>
                        <img src="/logo.svg" alt="Fundraiser" width="200" />
                        <Typography variant="headline">Registration form</Typography>
                        <form className={classes.form} onSubmit={this.formSubmit}>
                            <FormControl margin="normal" required error={!this.state.userName.isValid} fullWidth>
                                <InputLabel>Username:</InputLabel>
                                <Input className={this.state.userName.isValid ? "form-control" : "form-control is-invalid"}
                                    name="userName"
                                    value={this.state.userName.value}
                                    onChange={this.inputChangeHandler}
                                    placeholder="Username"
                                />
                                <FormHelperText id="component-error-text">{this.state.userName.message}</FormHelperText>
                            </FormControl>
                            <FormControl margin="normal" required fullWidth error={!this.state.email.isValid}>
                                <InputLabel htmlFor="email">Email Address</InputLabel>
                                <Input className={this.state.email.isValid ? "form-control" : "form-control is-invalid"}
                                    name="email"
                                    value={this.state.email.value}
                                    onChange={this.inputChangeHandler}
                                    placeholder="Email Address"
                                    type="text"
                                />
                                <FormHelperText id="component-error-text">{this.state.email.message}</FormHelperText>
                            </FormControl>
                            <FormControl margin="normal" required fullWidth error={!this.state.password.isValid}>
                                <InputLabel htmlFor="password">Password</InputLabel>
                                <Input className={this.state.password.isValid ? "form-control" : "form-control is-invalid"}
                                    name="password"
                                    value={this.state.password.value}
                                    onChange={this.inputChangeHandler}
                                    placeholder="Password"
                                    type="password"
                                />
                                <FormHelperText id="component-error-text">{this.state.password.message}</FormHelperText>
                            </FormControl>
                            <FormControl margin="normal" required fullWidth error={!this.state.confirmPassword.isValid}>
                                <InputLabel htmlFor="password">Confirm password</InputLabel>
                                <Input className={this.state.confirmPassword.isValid ? "form-control" : "form-control is-invalid"}
                                    name="confirmPassword"
                                    value={this.state.confirmPassword.value}
                                    onChange={this.inputChangeHandler}
                                    placeholder="Confirm password"
                                    type="password"
                                />
                                <FormHelperText id="component-error-text">{this.state.confirmPassword.message}</FormHelperText>
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <InputLabel htmlFor="currency-simple">Currency</InputLabel>
                                <Select
                                    value={this.state.currency.value}
                                    onChange={this.inputChangeHandler}
                                    inputProps={{
                                        name: 'currency',
                                        id: 'currency-simple',
                                    }}
                                >
                                    <MenuItem value="">
                                        <em>-- Choose a currency --</em>
                                    </MenuItem>
                                    <MenuItem value="USD">USD</MenuItem>
                                    <MenuItem value="EUR">EUR</MenuItem>
                                    <MenuItem value="GBP">GBP</MenuItem>
                                    <MenuItem value="HUF">HUF</MenuItem>
                                </Select>
                                <FormHelperText id="component-error-text">{this.state.currency.message}</FormHelperText>
                            </FormControl>
                            <Button
                                type="submit"
                                fullWidth
                                variant="raised"
                                color="primary"
                                className={classes.submit}
                            >
                                Create an account
                        </Button>
                        </form>
                    </Paper>
                </main>
            </React.Fragment>

        )
    }
}

RegistrationPage.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withSnackbar(withStyles(Styles.registrationStyle)((RegistrationPage)));