import React, {Component} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import classNames from 'classnames';
import {withStyles} from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import ProjectTable from "../ProjectTable/ProjectTable";
import Styles from '../../common/Styles';
import Urls from '../../common/Urls';
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import TabContainer from "../../components/TabContainer";
import SwipeableViews from "react-swipeable-views";
import ProjectCardsTab from "../ProjectCardsTab/ProjectCardsTab";
// import ProjectTable from "../../components/ProjectTable";

class AccountDashboardPage extends Component {
    MY_ACCOUNT_URL = '/api/accounts/myAccountDetails';

    state = {
        accountDetails: {
            projects: []
        },
        value: 0,
    };

    componentDidMount() {
        this.getAccountDetails();
    }

    getAccountDetails = () => {
        if (!localStorage.user) {
            this.props.history.replace('/login');
        } else {
            axios.get(Urls.account.list + "/myAccountDetails")
                .then((response) => {
                    console.log(response.data);
                    this.setState({
                        accountDetails: response.data
                    });
                })
                .catch(error => {
                    console.log(error);
                })

        }
    };

    deleteHandler = (id) => {
        console.log("Logging delete button id:", id);
        axios.delete(Urls.project.entity + "/" + id)
            .then(response => {
                console.log("Logging response:", response);
                this.getAccountDetails();
            })
            .catch(error => {
                console.log("Logging error:", error);
            });
    };

    handleChange = (event, value) => {
        this.setState({value});
    };

    handleChangeIndex = index => {
        this.setState({value: index});
    };

    render() {

        const {classes} = this.props;

        return (
            <React.Fragment>
                <CssBaseline/>
                <main>
                    <div className={classNames(classes.layout)}>
                        <Typography variant="headline" color="textPrimary" gutterBottom>
                            My account
                        </Typography>
                        <Divider/>
                        <br/>

                        <Grid container spacing={40} className={classes.cardGrid}>
                            <Grid item xs={12} md={3}>
                                <Typography align={"center"}>
                                    <i className="fas fa-user-circle user-dashboard"/>
                                </Typography>
                            </Grid>
                            <Grid item xs={12} md={9}>
                                <Typography
                                    variant="headline"><strong>{this.state.accountDetails.userName}</strong></Typography>
                                <Typography variant="subheading">
                                    Email address: {this.state.accountDetails.email} <br/>
                                    Balance: {this.state.accountDetails.balance} {this.state.accountDetails.currency}<br/>
                                </Typography>
                            </Grid>
                        </Grid>

                        <br/>
                        <br/>
                        <br/>

                        <Typography variant="headline" color="textPrimary" gutterBottom>
                            My project(s)
                        </Typography>
                        <Divider/>
                        <br/>

                        <Tabs value={this.state.value} onChange={this.handleChange} scrollable scrollButtons="off">
                            <Tab icon={<i className="fas fa-list"></i>}  label="Project list"  />
                            <Tab icon={<i className="fas fa-chart-line"></i>}   label="Statistics" />
                        </Tabs>


                        <SwipeableViews
                            // axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                            index={this.state.value}
                            onChangeIndex={this.handleChangeIndex}
                        >
                            {this.state.value === 0 && <TabContainer>
                                <ProjectTable
                                    projects={this.state.accountDetails.projects}
                                    deleteHandler={this.deleteHandler}
                                />
                            </TabContainer>}
                            {this.state.value === 1 && <TabContainer>
                                <ProjectCardsTab
                                    projects={this.state.accountDetails.projects}
                                    deleteHandler={this.deleteHandler}
                                />
                            </TabContainer>}
                        </SwipeableViews>

                    </div>
                </main>

            </React.Fragment>
        )
    }

}

AccountDashboardPage.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(Styles.accountDashboardPageStyle)(AccountDashboardPage);