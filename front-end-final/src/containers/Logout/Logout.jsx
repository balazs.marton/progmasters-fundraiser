import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import axios from "axios";

class Logout extends Component {
    componentDidMount() {
        axios.post('/logout')
            .then(response => {
                console.log(response);
                localStorage.removeItem('user');
                const anonymUser = {
                    name: "anonym",
                    role: "ROLE_ANONYMOUS"
                };
                localStorage.setItem('user', JSON.stringify(anonymUser));
                this.props.history.push('/');
            })
            .catch(error => {
                console.log(error.response);
            });
    }

    render() {
        // return <Redirect to='/login' />
        return <Redirect to='/' />
    }
}

export default Logout;