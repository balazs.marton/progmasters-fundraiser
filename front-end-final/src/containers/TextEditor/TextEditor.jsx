import React, {Component, Fragment} from 'react';
import RichTextEditor from 'react-rte';

class TextEditor extends Component {
    state = {
        value: RichTextEditor.createValueFromString(this.props.baseText, 'html')
    }

    onChange = (value) => {
        this.props.handleTextChange(value,this.props.id)
        this.setState({value});
    };


    render() {
        const toolbarConfig = {
            display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'BLOCK_TYPE_DROPDOWN'],
            INLINE_STYLE_BUTTONS: [
                {label: 'Bold', style: 'BOLD'},
                {label: 'Italic', style: 'ITALIC'},
                {label: 'Underline', style: 'UNDERLINE'}
            ],
            BLOCK_TYPE_DROPDOWN: [
                {label: 'Normal', style: 'unstyled'},
                {label: 'Heading Large', style: 'header-one'},
                {label: 'Heading Medium', style: 'header-two'},
                {label: 'Heading Small', style: 'header-three'}
            ],
            BLOCK_TYPE_BUTTONS: [
                {label: 'UL', style: 'unordered-list-item'},
                {label: 'OL', style: 'ordered-list-item'}
            ]
        };
        return (
            <Fragment>
                <RichTextEditor
                    toolbarConfig={toolbarConfig}
                    value={this.state.value}
                    onChange={this.onChange}
                />
            </Fragment>
        );
    }
}

export default TextEditor;