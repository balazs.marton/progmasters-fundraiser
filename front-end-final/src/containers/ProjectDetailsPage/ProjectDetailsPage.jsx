import React, {Component} from 'react';
// import RatingComponent from '../../components/RatingComponent';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import CssBaseline from '@material-ui/core/CssBaseline';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';

import axios from 'axios';
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import ProgressLine from "../../components/ProgressLine";
import DonateDialog from "../DonateDialog/DonateDialog";
import PermissionButton from "../../PermissionComponents/PermissionButtons/PermissionButton";
import Styles from '../../common/Styles'
import Urls from '../../common/Urls'

class ProjectDetailsPage extends Component {

    constructor(props) {
        super(props);
        document.title = 'Project details';
    }

    state = {
        project: {
            name: '',
            description: '',
            goal: 0,
            funds: 0,
            category: {},
            deadline: '',
            imageUrl: '',
            currency: ''
        },
        open: false,
    }

    componentDidMount() {
        this.getProjectDetails();
    }

    getProjectDetails = () => {
        axios.get(Urls.project.list+ '/' + this.props.match.params.id)
            .then(response => {
                console.log('válasz: ', response);
                this.setState({
                    project: response.data
                })
            })
    };

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({open: false});
        this.getProjectDetails();
    };

    calculateRaisedPercentage = () => {
        return (Math.floor((this.state.project.funds / this.state.project.goal) * 100) + '%');

    };

    hoverHandler = (id) => {
        console.log(id);
    };

    render() {
        const {classes} = this.props;

        return (
            <React.Fragment>
                <CssBaseline/>
                <main>
                    <div className={classNames(classes.layout)}>
                        <Typography variant="subheading" color="textPrimary" gutterBottom>
                            {this.state.project.category.name}
                        </Typography>
                        <Divider/>
                        <br/>

                        <Grid container spacing={40}>
                            <Grid item xs={12} md={6}>
                                <img className="projectImageDefault" src={this.state.project.imageUrl} alt="main"/>

                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Typography variant="headline">{this.state.project.name}</Typography>

                                <Typography variant="subheading" paragraph>
                                    {this.state.project.description}
                                </Typography>

                                <Typography variant="body1" paragraph>
                                    Project owner:<br/>
                                    {this.state.project.account}
                                </Typography>

                                <Typography variant="headline" >
                                    {this.state.project.funds} {this.state.project.currency}
                                </Typography>
                                <ProgressLine
                                    goal={this.state.project.goal}
                                    current={this.state.project.funds}
                                />
                                <Grid container spacing={40}>
                                    <Grid item xs={6}>
                                        <Typography variant="body1" paragraph>
                                           <this.calculateRaisedPercentage/> of {this.state.project.goal} {this.state.project.currency} goal
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Typography variant="body1" paragraph align={"right"}>
                                            {this.state.project.remainingDays}
                                        </Typography>

                                    </Grid>
                                </Grid>
                                <PermissionButton
                                    color="secondary"
                                    variant="contained"
                                    onClick={this.handleClickOpen}
                                    role={["ROLE_USER", "ROLE_ADMINISTRATOR"]}
                                >
                                    Donate
                                </PermissionButton>


                                
                                <Dialog
                                    open={this.state.open}
                                    onClose={this.handleClose}
                                    aria-labelledby="form-dialog-title"
                                >
                                    <DialogContent>
                                        <DonateDialog
                                            onClick={this.handleClose}
                                            projectId={this.props.match.params.id}
                                        />
                                    </DialogContent>
                                </Dialog>
                            </Grid>
                        </Grid>
                    </div>
                </main>

            </React.Fragment>
        )
    }
}

ProjectDetailsPage.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(Styles.projectDetailsStyle)(ProjectDetailsPage);