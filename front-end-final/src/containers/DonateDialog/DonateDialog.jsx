import React, {Component} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Styles from '../../common/Styles'
import Urls from '../../common/Urls'

class DonateDialog extends Component {
    state = {
        formData: {
            amount: {
                value: "",
                isValid: true,
                message: null
            }
        }
    };

    inputChangeHandler = event => {
        const value = event.target.value;
        const fieldName = event.target.name;
        const updatedFormData = {...this.state.formData};

        updatedFormData[fieldName] = {
            value: value,
            isValid: true,
            message: null
        }

        this.setState({formData: updatedFormData})
    };

    formSubmit = event => {
        event.preventDefault();
        const data = {
            amount: this.state.formData.amount.value,
            projectId: this.props.projectId
        };
    console.log(data);
        axios.post(Urls.transfer.entity, data)
            .then(() => {
                this.props.onClick();
            })
            .catch(error => {
                console.warn("error.response: ", error === null ? error.response : error);
                console.log("error.response.data: ", error === null ? error : error);
                this.validationHandler(error);
            });
    }

    validationHandler = error => {
        const responseData = error.response.data;
        const updatedFormData = {...this.state.formData};
        if (responseData !== null) {
            if (responseData.globalError) {
                this.setState({globalMessage: responseData.globalError});
            }
            if (responseData.fieldErrors) {
                for (let fe of responseData.fieldErrors) {
                    updatedFormData[fe.field] = {
                        value: this.state.formData[fe.field].value,
                        isValid: false,
                        message: fe.message
                    }
                }
            }
            this.setState({formData: updatedFormData});
        } else {
            this.setState({globalMessage: error});
        }
    };

    render() {
        const {classes} = this.props;

        return (
            <React.Fragment>
                <CssBaseline/>
                <Typography variant="headline">Donation</Typography>
                <Typography variant="subheading">Please, enter the amount</Typography>
                <form className={classes.form} onSubmit={this.formSubmit}>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="amount">Amount</InputLabel>
                        <Input
                            name="amount"
                            type="number"
                            id="amount"
                            autoFocus
                            onChange={this.inputChangeHandler}
                            value={this.state.formData.amount.value}
                        />
                    </FormControl>
                    <Button
                        type="submit"
                        fullWidth
                        variant="raised"
                        color="secondary"
                        className={classes.submit}
                    >
                        Donate
                    </Button>
                </form>
            </React.Fragment>
        )
    };

};

DonateDialog.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(Styles.donateDialogStyle)(DonateDialog);