import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import {withStyles} from '@material-ui/core/styles';
import ProjectListItem from '../../components/ProjectListItem';
import Button from '@material-ui/core/Button';

import axios from 'axios';
import Styles from '../../common/Styles'
import Urls from '../../common/Urls'

class ProjectListPage extends Component {

    constructor(props) {
        super(props);
        document.title = 'Project list';
    }

    CATEGORY = "Category";
    DAYS_UNTIL_DEADLINE = "Days until deadline";
    FUNDS_RAISED = "Funds raised";

    state = {
        categories: [],
        projects: [],
        categoryFilter: -1,
        remainingTimeFilter: -1,
        fundsFilter: -1,
    };

    componentDidMount() {
        this.getProjects();

        axios.get(Urls.category.list + "/is-used")
            .then(response => {
                this.setState({
                    categories: response.data
                })
            })
            .catch(error => {
                console.log("Logging error in componentDidMount:", error);
            });
    }

    getProjects = () => {
        axios.get(Urls.project.list)
            .then(response => {
                this.setState({
                    projects: response.data
                })
            })
            .catch(error => {
                console.log("Logging error in componentDidMount:", error);
            });
    };

    filterProjects = (event) => {

        const data = {
            ...this.state
        };

        data[event.target.name] = event.target.value;

        let url = "";

        if (data.categoryFilter !== -1) {
            if (!url) {
                url += "?";
            } else {
                url += "&";
            }
            url += "category=" + data.categoryFilter;
        }
        if (data.remainingTimeFilter !== -1) {
            if (!url) {
                url += "?";
            } else {
                url += "&";
            }
            url += "remainingTime=" + data.remainingTimeFilter;
        }
        if (data.fundsFilter !== -1) {
            if (!url) {
                url += "?";
            } else {
                url += "&";
            }
            url += "funds=" + data.fundsFilter;
        }

        console.log("URL:", url);

        if (url) {
            axios.get(Urls.project.list + "/filter" + url)
                .then(response => {
                    this.setState({
                        projects: response.data
                    })
                })
                .catch(error => {
                    console.log("Logging error:", error);
                });
        } else {
            this.getProjects();
        }
    };

    filterHandler = (event) => {
        const target = event.target.name;
        let value = event.target.value;

        let newState = {
            ...this.state
        };

        newState[target] = value;

        this.setState({
            ...newState
        });
    };

    render() {
        const {classes} = this.props;

        return (
            <React.Fragment>
                <CssBaseline/>
                <main className={classNames(classes.layout)}>
                    <div className={classes.layout}>
                        <InputLabel htmlFor="text"><strong>Filter: </strong></InputLabel>
                        <Select
                            className={classes.filter}
                            value={this.state.categoryFilter}
                            onChange={this.filterHandler}
                            inputProps={{
                                name: 'categoryFilter',
                                id: 'category-simple',
                            }}
                        >

                            <MenuItem value={-1}>{this.CATEGORY}</MenuItem>
                            {this.state.categories.map((category, index) => {
                                return (
                                    <MenuItem
                                        key={index}
                                        value={category.id} placeholder={this.state.categoryFilter}>-{category.name}</MenuItem>
                                )
                            })}
                        </Select>
                        <Select
                            className={classes.filter}
                            value={this.state.remainingTimeFilter}
                            onChange={this.filterHandler}
                            inputProps={{
                                name: 'remainingTimeFilter',
                                id: 'remainingTimeFilter-simple',
                            }}
                        >

                            <MenuItem value={-1}>{this.DAYS_UNTIL_DEADLINE}</MenuItem>
                            <MenuItem value={0}>Raise is over</MenuItem>
                            <MenuItem value="1-5">1 - 5</MenuItem>
                            <MenuItem value="6-15">6 - 15</MenuItem>
                            <MenuItem value="16-30">16 - 30</MenuItem>
                            <MenuItem value="31-50">31 - 50</MenuItem>
                            <MenuItem value="51+">50 +</MenuItem>
                        </Select>
                        <Select
                            className={classes.filter}
                            value={this.state.fundsFilter}
                            onChange={this.filterHandler}
                            inputProps={{
                                name: 'fundsFilter',
                                id: 'fundsFilter-simple',
                            }}
                        >

                            <MenuItem value={-1}>{this.FUNDS_RAISED}</MenuItem>
                            <MenuItem value="0-20">0 - 20%</MenuItem>
                            <MenuItem value="21-40">21 - 40%</MenuItem>
                            <MenuItem value="41-60">41 - 60%</MenuItem>
                            <MenuItem value="61-80">61 - 80%</MenuItem>
                            <MenuItem value="81+">81 - 100%</MenuItem>
                        </Select>
                        <Button
                            variant="contained" color="secondary"
                            className={classes.button}
                            onClick={this.filterProjects}
                        >Filter</Button>
                        <br/>
                        <br/>
                        <Grid container spacing={40}>
                            {this.state.projects.map((project) =>
                                <ProjectListItem
                                    key={project.id}
                                    id={project.id}
                                    name={project.name}
                                    imageUrl={project.imageUrl}
                                    description={project.description}
                                    remainingDays={project.remainingDays}
                                    funds={project.funds}
                                    goalFunds={project.goalFunds}
                                />
                            )}
                        </Grid>
                    </div>
                </main>

            </React.Fragment>
        );
    }
}

ProjectListPage.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(Styles.projectListPageStyle)(ProjectListPage);