import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CssBaseline from "@material-ui/core/CssBaseline";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "../../../node_modules/@material-ui/icons/Edit";
import classNames from "classnames";
import PropTypes from "prop-types";
import {withStyles} from "@material-ui/core/styles";
import Styles from '../../common/Styles';
import axios from "axios";
import Urls from "../../common/Urls";
import TextEditor from "../TextEditor/TextEditor";


class EmailTable extends Component {
    state = {
        open: false,
        rows: {},
        emails: [],
        email: {
            id: 0,
            value: null,
        }
    };

    componentDidMount() {
        this.getEmails();
    }

    getEmails = () => {
        axios.get(Urls.email.list)
            .then(response => {
                // console.log(response);
                this.setState({
                    emails: response.data
                })
            })
    };

    handleClickOpen = (emailId) => {
        let updateRows = this.state.rows;
        updateRows[emailId] = true;
        this.setState(updateRows);
        // console.log(this.state.rows)
    };

    handleClose = (emailId) => {
        let updateRows = this.state.rows;
        updateRows[emailId] = false;
        this.setState(updateRows);
    };

    handleEmailTextChange = (text, id) => {


        this.setState(
            {
                email: {
                    id: id,
                    value: text
                }
            });
    };

    saveEmail = () => {

        const data = {
            // id: this.state.email.id,
            id: this.state.email.id,
            emailBody: this.state.email.value.toString('html'),
        };

        axios.put(Urls.email.entity, data)
            .then(this.getEmails)
            .catch(error => {
                // console.warn("error.response: ", error === null ? error.response : error);
                // console.log("error.response.data: ", error === null ? error : error);
                // this.validationHandler(error);
            });
    };

    render() {
        const {classes} = this.props;

        return (
            <React.Fragment>
                <CssBaseline/>
                <Table className={classNames(classes.table)}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Email name</TableCell>
                            <TableCell numeric></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.emails.map(email => {


                            return (
                                <TableRow key={email.id}>
                                    <TableCell component="th" scope="row">
                                        <Typography className="projectTableLink" variant="body1" color="inherit">
                                            {email.templateName}
                                        </Typography>

                                    </TableCell>
                                    <TableCell numeric>
                                        <IconButton color="inherit" onClick={() => {
                                            this.handleClickOpen(email.id);
                                        }}>
                                            <EditIcon/>
                                        </IconButton>
                                        <Dialog
                                            open={this.state.rows[email.id]}
                                            onClose={() => {
                                                this.handleClose(email.id);
                                            }}
                                            aria-labelledby="texteditor-dialog-title"
                                            aria-describedby="texteditor-dialog-description"
                                        >
                                            <DialogTitle id="texteditor-dialog-title">Edit email</DialogTitle>
                                            <DialogContent>
                                                <DialogContentText>
                                                    You can use the following parameters to make the email
                                                    personalised: {email.allowedParameter}
                                                </DialogContentText>
                                                <TextEditor
                                                    baseText={email.emailBody}
                                                    id={email.id}
                                                    handleTextChange={this.handleEmailTextChange}
                                                />
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={() => {
                                                    this.handleClose(email.id);
                                                }}>
                                                    Cancel
                                                </Button>
                                                <Button onClick={
                                                    () => {
                                                        this.saveEmail(email.id);
                                                        this.handleClose(email.id);
                                                    }
                                                }
                                                        autoFocus>
                                                    Save
                                                </Button>
                                            </DialogActions>
                                        </Dialog>

                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </React.Fragment>
        );
    }
}

EmailTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(Styles.projectTableStyle)(EmailTable);