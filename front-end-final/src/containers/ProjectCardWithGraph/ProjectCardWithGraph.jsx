import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';
import ProgressLine from "../../components/ProgressLine";
import {Link} from "react-router-dom";

import classnames from 'classnames';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SimpleLineChart from "../../components/SimpleLineChart";
import axios from "axios";
import Urls from "../../common/Urls";


const styles = theme => ({
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    actions: {
        display: 'flex',
    },
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
        marginLeft: 'auto',
        [theme.breakpoints.up('sm')]: {
            marginRight: -8,
        },
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
});

class ProjectCardWithGraph extends Component {
    state = {
        expanded: false,
        transfers: [],
    };

    componentDidMount() {
        axios.get(Urls.transfer.list + "/" + this.props.project.id)
            .then(response => {
                console.log('Transzferek: ', response.data);
                this.setState({
                    transfers: response.data
                })
            })
            .catch(error => {
                console.log("Logging error in componentDidMount:", error);
            });
    }

    handleExpandClick = () => {
        let updateState = {...this.state};
        updateState.expanded = !this.state.expanded;
        this.setState(updateState);
    };

    render() {
        const {classes} = this.props;

        const projectUrl = '/projects/' + this.props.project.id;
        let raisedPercent = Math.floor((this.props.project.funds / this.props.project.goalFunds) * 100) + '%';

        return (
            <Fragment>
                <CssBaseline/>
                <Grid item xs={12}>
                    <Card className={classes.card}>
                        <CardContent className={classes.cardContent}>
                            <Typography className="projectDetailsLink" gutterBottom variant="headline">
                                <Link to={projectUrl}> {this.props.project.name} </Link>
                            </Typography>
                            <Typography align="right">
                                {this.props.project.remainingDays}<br/>
                                {raisedPercent} raised
                            </Typography>
                            <ProgressLine
                                goal={this.props.project.goalFunds}
                                current={this.props.project.funds}
                            />
                        </CardContent>
                        <CardActions className={classes.actions} disableActionSpacing>
                            <IconButton
                                className={classnames(classes.expand, {
                                    [classes.expandOpen]: this.state.expanded,
                                })}
                                onClick={this.handleExpandClick}
                                aria-expanded={this.state.expanded}
                                aria-label="Show more"
                            >
                                <ExpandMoreIcon/>
                            </IconButton>
                        </CardActions>
                        <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
                            <CardContent>
                                <Typography paragraph variant="body2">
                                    Graph of the donations:
                                </Typography>
                                <SimpleLineChart
                                    transfers={this.state.transfers}
                                    createDate={this.props.project.createDate}
                                />
                            </CardContent>
                        </Collapse>
                    </Card>
                </Grid>
            </Fragment>
        );
    }
}

ProjectCardWithGraph.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProjectCardWithGraph);