import React, {Component} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import { withSnackbar } from 'notistack';

import Styles from '../../common/Styles'
import Urls from '../../common/Urls'

class CreateCategoryPage extends Component {
    state = {
        formData: {
            name: {
                value: "",
                isValid: true,
                message: null
            }
        }
    };

    inputChangeHandler = event => {
        const value = event.target.value;
        const fieldName = event.target.name;
        const updatedFormData = {...this.state.formData};

        updatedFormData[fieldName] = {
            value: value,
            isValid: true,
            message: null
        }

        this.setState({formData: updatedFormData})
    };

    formSubmit = event => {
        event.preventDefault();
        const data = {
            name: this.state.formData.name.value,
        };
        const { onPresentSnackbar } = this.props;

        axios.post(Urls.category.entity, data)
            .then(() => onPresentSnackbar('success', 'Category saved'))
            .then(() => {
                this.props.onClick();
            })
            .catch(error => {
                console.warn("error.response: ", error === null ? error.response : error);
                console.log("error.response.data: ", error === null ? error : error);
                this.validationHandler(error);
            });
    }

    validationHandler = error => {
        const responseData = error.response.data;
        const updatedFormData = {...this.state.formData};
        if (responseData !== null) {
            if (responseData.globalError) {
                this.setState({globalMessage: responseData.globalError});
            }
            if (responseData.fieldErrors) {
                for (let fe of responseData.fieldErrors) {
                    updatedFormData[fe.field] = {
                        value: this.state.formData[fe.field].value,
                        isValid: false,
                        message: fe.message
                    }
                }
            }
            this.setState({formData: updatedFormData});
        } else {
            this.setState({globalMessage: error});
        }
    };

    render() {
        const {classes} = this.props;

        return (
            <React.Fragment>
                <CssBaseline/>
                <Typography variant="headline">Create Category</Typography>
                <form className={classes.form} onSubmit={this.formSubmit}>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="name">Name</InputLabel>
                        <Input
                            name="name"
                            type="text"
                            id="name"
                            autoFocus
                            onChange={this.inputChangeHandler}
                            value={this.state.formData.name.value}
                        />
                    </FormControl>
                    <Button
                        type="submit"
                        fullWidth
                        variant="raised"
                        color="primary"
                        className={classes.submit}
                    >
                        Create a Category
                    </Button>
                </form>
            </React.Fragment>
        )
    };

};

CreateCategoryPage.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withSnackbar(withStyles(Styles.createCategoryPageStyle)(CreateCategoryPage));