import React from 'react';
import '../App.css';
import {withStyles} from '@material-ui/core/styles';
import classNames from 'classnames';
import IconButton from "@material-ui/core/es/IconButton/IconButton";

const styles = theme => ({
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1280 + theme.spacing.unit * 3 * 2)]: {
            width: 1280,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
        mainGrid: {
            marginTop: theme.spacing.unit * 3,
        },
        markdown: {
            padding: `${theme.spacing.unit * 3}px 0`,
        },
    },
});

function RatingComponent(props) {
    const {classes} = props;

    return (
        <div className={classNames(classes.layout)}>
            <IconButton color="inherit">
                <i className="star far fa-star"/>
            </IconButton>
            <IconButton color="inherit">
                <i className="star far fa-star"/>
            </IconButton>
            <IconButton color="inherit">
                <i className="star far fa-star"/>
            </IconButton>
            <IconButton color="inherit">
                <i className="star far fa-star"/>
            </IconButton>
            <IconButton color="inherit">
                <i className="star far fa-star"/>
            </IconButton>
        </div>
    )
}

export default withStyles(styles)(RatingComponent);