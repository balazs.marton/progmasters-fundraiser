import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';
import Fade from "@material-ui/core/Fade";
import ProgressLine from "./ProgressLine";
import {Link} from "react-router-dom";


const styles = theme => ({
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing.unit * 6,
    },
});

function ProjectListItem(props) {
    const {classes} = props;

    const projectUrl = '/projects/' + props.id;

    let raisedPercent = Math.floor((props.funds / props.goalFunds) * 100) + '%';

    return (
        <React.Fragment>
            <CssBaseline/>
            <Fade in timeout={500}>
                <Grid item key={props.id} sm={6} md={4}>
                    <Card className={classes.card}>
                        <CardMedia
                            className={classes.cardMedia}
                            image={props.imageUrl} // eslint-disable-line max-len
                            title="title"
                            component={Link}
                            to={projectUrl}
                        />
                        <CardContent className={classes.cardContent}>
                            <Typography className="projectDetailsLink" gutterBottom variant="headline" component="h2">
                                <Link to={projectUrl}> {props.name} </Link>
                            </Typography>
                            <Typography gutterBottom>
                                {props.description}<br/><br/>
                            </Typography>
                            <Typography>
                                {raisedPercent} raised
                            </Typography>
                                <ProgressLine
                                    goal={props.goalFunds}
                                    current={props.funds}
                                />
                            <Typography align="right">
                                {props.remainingDays}
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
            </Fade>
        </React.Fragment>
    );
}

ProjectListItem.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProjectListItem);