import { Redirect } from 'react-router-dom';
import React from 'react';

function RedirectSearchBar(props) {
    return <Redirect to={"/projects/" +  props.match.params.id} />

}


export default RedirectSearchBar