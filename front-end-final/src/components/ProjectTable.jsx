import React from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import {withStyles} from '@material-ui/core/styles';
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Table from "@material-ui/core/Table";
import {Link} from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "../../node_modules/@material-ui/icons/Edit";
import DeleteIcon from "../../node_modules/@material-ui/icons/Delete";
import ProgressLine from "./ProgressLine";
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    table: {
        minWidth: 700,
    },
});


function ProjectTable(props) {
    console.log('projektek: ', props);

    const {classes} = props;

    return (
        <React.Fragment>
            <CssBaseline/>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell>Project name</TableCell>
                        <TableCell numeric>Raised funds / Goal</TableCell>
                        <TableCell numeric>Remaining</TableCell>
                        <TableCell numeric></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.projects.map(project => {

                        const editUrl = '/projects/' + project.id + '/edit';
                        const projectUrl = '/projects/' + project.id ;

                        return (
                            <TableRow key={project.id}>
                                <TableCell component="th" scope="row">
                                    <Typography className="projectTableLink" variant="body1" color="inherit">
                                        <Link to={projectUrl}> {project.name}</Link>
                                    </Typography>

                                </TableCell>
                                <TableCell numeric>
                                    {project.funds} / {project.goalFunds}<br/>
                                    <ProgressLine
                                        goal={project.goalFunds}
                                        current={project.funds}
                                    />
                                </TableCell>
                                <TableCell numeric>{project.remainingDays}</TableCell>
                                <TableCell numeric>
                                    <IconButton color="inherit" component={Link} to={editUrl}>
                                        <EditIcon/>
                                    </IconButton>
                                    <IconButton color="inherit" onClick={() => props.deleteHandler(project.id)}>
                                        <DeleteIcon/>
                                    </IconButton>

                                </TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </React.Fragment>
    );
}

ProjectTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProjectTable);