import React from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';
import {Link} from 'react-router-dom';
import Paper from "@material-ui/core/Paper";

const styles = theme => ({
    layout: {
        width: 'auto',
        display: 'block', // Fix IE11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(500 + theme.spacing.unit * 3 * 2)]: {
            width: 500,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },

});

function PageNotFound(props) {
    const {classes} = props;

    return (
        <React.Fragment>
            <CssBaseline/>
            <main className={classes.layout}>
                <Paper className={classes.paper}>
                    <Typography variant="headline">Houston, we've had a problem here</Typography>
                    <img src="/404.svg" alt="404" width="300"/>
                    <Typography variant="headline" align="center"> The page that you're looking for was not found</Typography>
                    <br/>
                    <Typography variant="subheading" component={Link} to={'/'}> Back to the homapage </Typography>

                </Paper>
            </main>
        </React.Fragment>
    );
}

PageNotFound.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PageNotFound);