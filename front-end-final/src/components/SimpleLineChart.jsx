import React from 'react';
import ResponsiveContainer from 'recharts/lib/component/ResponsiveContainer';
import ComposedChart from 'recharts/lib/chart/ComposedChart';
import Line from 'recharts/lib/cartesian/Line';
import Bar from 'recharts/lib/cartesian/Bar';
import XAxis from 'recharts/lib/cartesian/XAxis';
import YAxis from 'recharts/lib/cartesian/YAxis';
import CartesianGrid from 'recharts/lib/cartesian/CartesianGrid';
import Tooltip from 'recharts/lib/component/Tooltip';
import Legend from 'recharts/lib/component/Legend';
import Moment from 'moment';

const initData = (createDate, lastDate) => {
    let data = [];
    for (let d = createDate; d <= lastDate; d.setDate(d.getDate() + 1)) {
        data.push({
            oneDayFunds: 0,
            date: Moment(d).format(('YYYY-MM-DD')),
        });
    }
    return data;
}

const implData = (data, transfers) => {
    let raised = 0;
    data.map(item => {
        item.raised = raised;
         transfers.map(transfer => {
              if(item.date === transfer.date){
                  item.oneDayFunds = transfer.oneDayFunds;
                  raised = raised + transfer.oneDayFunds;
                  item.raised = raised;
              }
         })
    });
};

function SimpleLineChart(props) {

    let createDate = new Date(props.createDate);
    let lastDate = new Date();
    let data = initData(createDate, lastDate);
    let transfers = props.transfers;
    implData(data, transfers);

    return (
        <ResponsiveContainer width="99%" height={320}>
            <ComposedChart data={data}>
                <CartesianGrid stroke='#f5f5f5'/>
                <XAxis dataKey="date"/>
                <YAxis/>
                <Tooltip itemStyle={{ color: 'black' }} />
                <Legend/>
                <Bar name="Donations" dataKey='oneDayFunds' barSize={20} fill='#ffab40'/>
                <Line name="Raised" type='monotone' dataKey='raised'  strokeWidth={3} stroke='#ffab40'/>
            </ComposedChart>
        </ResponsiveContainer>
    );
}

export default SimpleLineChart;