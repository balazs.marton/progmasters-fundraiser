import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
});

function ProgressLine(props) {
    const {classes} = props;

    let percent = 0;

    if (props.goal >= props.current) {
        percent = (props.current / props.goal) * 100;
    } else {
        percent = 100;
    }

    return (
        <div className={classes.root}>
            <LinearProgress variant="determinate" color={"secondary"} value={percent}/>
        </div>
    );
}

ProgressLine.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProgressLine);