import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import axios from 'axios';


if (JSON.parse(localStorage.getItem('user')) === undefined ||
    JSON.parse(localStorage.getItem('user')) === null) {
    const anonymUser = {
        name: "anonym",
        role: "ROLE_ANONYMOUS"
    }
    localStorage.setItem('user', JSON.stringify(anonymUser));
}
axios.defaults.baseURL = 'http://localhost:8080';
axios.defaults.withCredentials = true;
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
