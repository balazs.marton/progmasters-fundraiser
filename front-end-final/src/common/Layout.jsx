import React from 'react';
import {Route, Switch} from 'react-router-dom';
import NavBar from './Navbar';
import LoginPage from "../containers/LoginPage/LoginPage";
import Footer from "./Footer";
import HomePage from "../containers/HomePage/HomePage";
import Registration from "../containers/RegistrationPage/RegistrationPage";
import PrivacyPolicy from "./PrivacyPolicyPage";
import PageNotFound from "../components/PageNotFound";
import ProjectList from "../containers/ProjectListPage/ProjectListPage";
import ProjectForm from "../containers/ProjectForm/ProjectForm";
import AccountDashboardPage from '../containers/AccountDashboardPage/AccountDashboardPage';
import AdminDashboardPage from "../containers/AdminDashboardPage/AdminDashboardPage";
import ProjectDetailsPage from "../containers/ProjectDetailsPage/ProjectDetailsPage";
import Logout from '../containers/Logout/Logout';
import ContactUsPage from "./ContactUsPage";
import TermOfUsePage from "./TermOfUsePage";
import RedirectSearchBar from '../components/RedirectSearchBar';
import {SnackbarProvider} from 'notistack';


function Layout() {
    return (
        <div>
            <NavBar/>
            <div style={{paddingTop: 74}}>
                <SnackbarProvider maxSnack={3} anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}>
                    <Switch>
                        <Route path='/' exact component={HomePage}/>
                        <Route path="/login" exact component={LoginPage}/>
                        <Route path="/logout" exact component={Logout}/>
                        <Route path="/projects" exact component={ProjectList}/>
                        <Route path="/projects/:id" exact component={ProjectDetailsPage}/>
                        <Route path="/redirectSearchBar/:id" exact component={RedirectSearchBar}/>
                        <Route path="/admin" exact component={AdminDashboardPage}/>
                        <Route path="/create-project" exact component={ProjectForm}/>
                        <Route path="/projects/:id/edit" exact component={ProjectForm}/>
                        <Route path="/registration" exact component={Registration}/>
                        <Route path="/privacy-policy" exact component={PrivacyPolicy}/>
                        <Route path="/contact-us" exact component={ContactUsPage}/>
                        <Route path="/term-of-use" exact component={TermOfUsePage}/>
                        <Route path="/account-dashboard" exact component={AccountDashboardPage}/>
                        <Route path="/projects/:id/edit" exact component={ProjectForm}/>
                        <Route path="/accountDashboard" exact component={AccountDashboardPage}/>
                        <Route component={PageNotFound}/>
                    </Switch>
                </SnackbarProvider>
            </div>
            <Footer/>
        </div>
    );
}

export default Layout;