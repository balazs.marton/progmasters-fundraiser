import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';
import {Link} from 'react-router-dom';

const styles = theme => ({
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1280 + theme.spacing.unit * 3 * 2)]: {
            width: 1280,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    footer: {
        marginTop: theme.spacing.unit * 8,
        borderTop: `1px solid ${theme.palette.divider}`,
        padding: `${theme.spacing.unit * 6}px 0`,
    },
});

function Footer(props) {
    const {classes} = props;

    return (
        <div className='container'>
            <CssBaseline/>
            {/* Footer */}
            <footer className={classNames(classes.footer, classes.layout)}>
                <Grid container spacing={32} justify="space-evenly">
                    <Grid item xs={6} >
                        <Typography variant="title" color="textPrimary" gutterBottom>
                            Company
                        </Typography>
                        <Typography variant="subheading" color="textSecondary" component={Link} to="/contact-us">
                            Contact us
                        </Typography>
                    </Grid>
                    <Grid item xs={6} >
                        <Typography variant="title" color="textPrimary" gutterBottom>
                            Legal
                        </Typography>
                        <Typography variant="subheading" color="textSecondary" component={Link} to="/privacy-policy">
                            Privacy policy
                        </Typography>
                        <Typography variant="subheading" color="textSecondary"component={Link} to="/term-of-use">
                            Terms of use
                        </Typography>
                    </Grid>
                </Grid>
            </footer>
            {/* End footer */}
        </div>
    );
}

Footer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);