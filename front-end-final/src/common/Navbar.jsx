import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Menu from '@material-ui/core/Menu';
import { withStyles } from '@material-ui/core/styles';
import AccountCircle from '@material-ui/icons/AccountCircle';
import SettingsIcon from '@material-ui/icons/Settings';
import { Link } from 'react-router-dom';
import PermissionButton from '../PermissionComponents/PermissionButtons/PermissionButton';
import PermissionIconButton from '../PermissionComponents/PermissionButtons/PermissionIconButton';
import PermissionMenuItem from '../PermissionComponents/PermissionMenuItem/PermissionMenuItem';
import SearchBar from './SearchBar';

const styles = theme => ({
    root: {
        width: '100%',
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    button: {
        marginLeft: -12,
        marginRight: 20,
    },
    title: {
        marginRight: 20,
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
});

class Navbar extends React.Component {
    state = {
        anchorEl: null,
        mobileMoreAnchorEl: null,
    };


    handleProfileMenuOpen = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleMenuClose = () => {
        this.setState({ anchorEl: null });
        this.handleMobileMenuClose();
    };

    handleMobileMenuOpen = event => {
        this.setState({ mobileMoreAnchorEl: event.currentTarget });
    };

    handleMobileMenuClose = () => {
        this.setState({ mobileMoreAnchorEl: null });
    };

    render() {
        const { anchorEl } = this.state;
        const { classes } = this.props;
        const isMenuOpen = Boolean(anchorEl);

        const renderMenu = (
            <Menu
                anchorEl={anchorEl}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={isMenuOpen}
                onClose={this.handleMenuClose}
            >
                    <PermissionMenuItem
                        onClick={this.handleMenuClose}
                        component={Link}
                        className={classes.button}
                        to="/login"
                        role={["ROLE_ANONYMOUS"]}
                        >
                        Login
                        </PermissionMenuItem>
                    <PermissionMenuItem
                        onClick={this.handleMenuClose}
                        component={Link} className={classes.button}
                        to="/registration"
                        role={["ROLE_ANONYMOUS"]}
                    >
                        Registration
                   </PermissionMenuItem>
                    <PermissionMenuItem
                        onClick={this.handleMenuClose}
                        component={Link}
                        to="/account-dashboard"
                        role={["ROLE_USER"]}
                    >
                        My account
                    </PermissionMenuItem>
                    <PermissionMenuItem
                        onClick={this.handleMenuClose}
                        component={Link}
                        to="/logout"
                        role={["ROLE_USER", "ROLE_ADMINISTRATOR"]}
                    >
                        Logout
                    </PermissionMenuItem>
            </Menu>
        );
        return (
            <div className={classes.root}>

                <AppBar position="fixed">
                    <Toolbar>
                        <Typography className={classes.title} variant="title" color="inherit" noWrap component={Link}
                            to="/">
                            <img src="/logo-horizontal-dark.svg" alt="Fundraiser" height="30" />
                        </Typography>

                        <div className={classes.grow} />
                        <div className={classes.sectionDesktop}>

                            <PermissionButton
                                color="secondary" variant="contained"
                                component={Link}
                                className={classes.button}
                                to="/create-project"
                                role={["ROLE_USER"]}
                            >
                                Create Project
                            </PermissionButton>
                            <SearchBar/>
                            <PermissionIconButton
                                color="inherit"
                                component={Link}
                                to="/projects"
                                role={["ROLE_ANONYMOUS", "ROLE_USER", "ROLE_ADMINISTRATOR"]}
                            >
                                <i className="fas fa-rocket" />
                            </PermissionIconButton>
                            <PermissionIconButton
                                aria-owns={isMenuOpen ? 'material-appbar' : null}
                                aria-haspopup="true"
                                onClick={this.handleProfileMenuOpen}
                                color="inherit"
                                role={["ROLE_ANONYMOUS", "ROLE_USER", "ROLE_ADMINISTRATOR"]}
                            >
                                <AccountCircle />
                            </PermissionIconButton>
                            <PermissionIconButton
                                color="inherit"
                                component={Link} to="/admin"
                                role={["ROLE_ADMINISTRATOR"]}
                            >
                                <SettingsIcon />
                            </PermissionIconButton>
                        </div>

                    </Toolbar>
                </AppBar>
                {renderMenu}
            </div>
        );
    }
}

Navbar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Navbar);