import { emphasize } from '@material-ui/core/styles/colorManipulator';

export default {

    registrationStyle: theme => 
        ({
            layout: {
                width: 'auto',
                display: 'block', // Fix IE11 issue.
                marginLeft: theme.spacing.unit * 3,
                marginRight: theme.spacing.unit * 3,
                [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
                    width: 400,
                    marginLeft: 'auto',
                    marginRight: 'auto',
                },
            },
            paper: {
                marginTop: theme.spacing.unit * 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
            },
            form: {
                width: '100%', // Fix IE11 issue.
                marginTop: theme.spacing.unit,
            },
            submit: {
                marginTop: theme.spacing.unit * 3,
            },
            root: {
                display: 'flex',
                flexWrap: 'wrap',
              },
            formControl: {
                margin: theme.spacing.unit,
                minWidth: 120,
              },
            selectEmpty: {
                marginTop: theme.spacing.unit * 2,
              },
        }),
    
    loginStyle: theme => ({
        layout: {
            width: 'auto',
            display: 'block', // Fix IE11 issue.
            marginLeft: theme.spacing.unit * 3,
            marginRight: theme.spacing.unit * 3,
            [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
                width: 400,
                marginLeft: 'auto',
                marginRight: 'auto',
            },
        },
        paper: {
            marginTop: theme.spacing.unit * 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
        },
        avatar: {
            margin: theme.spacing.unit,
            backgroundColor: theme.palette.secondary.main,
        },
        form: {
            width: '100%', // Fix IE11 issue.
            marginTop: theme.spacing.unit,
        },
        submit: {
            marginTop: theme.spacing.unit * 3,
        },
    }),
    
    homePageStyle: theme => ({
        icon: {
            marginRight: theme.spacing.unit * 2,
        },
        heroContent: {
            maxWidth: 600,
            margin: '0 auto',
            padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
        },
        heroButtons: {
            marginTop: theme.spacing.unit * 4,
        },
        layout: {
            width: 'auto',
            marginLeft: theme.spacing.unit * 3,
            marginRight: theme.spacing.unit * 3,
            [theme.breakpoints.up(1280 + theme.spacing.unit * 3 * 2)]: {
                width: 1280,
                marginLeft: 'auto',
                marginRight: 'auto',
            },
        },
        cardGrid: {
            padding: `${theme.spacing.unit * 8}px 0`,
        },
        card: {
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
        },
        cardMedia: {
            paddingTop: '56.25%', // 16:9
        },
        cardContent: {
            flexGrow: 1,
        },
        footer: {
            backgroundColor: theme.palette.background.paper,
            padding: theme.spacing.unit * 6,
        },
    }), 

    projectDetailsStyle: theme => ({
        layout: {
            width: 'auto',
            marginLeft: theme.spacing.unit * 3,
            marginRight: theme.spacing.unit * 3,
            [theme.breakpoints.up(1280 + theme.spacing.unit * 3 * 2)]: {
                width: 1280,
                marginLeft: 'auto',
                marginRight: 'auto',
                marginTop: '20px',
            },
            mainGrid: {
                marginTop: theme.spacing.unit * 3,
            },
            markdown: {
                padding: `${theme.spacing.unit * 3}px 0`,
            },
        },
    }),

    projectFormStyle: theme => ({
        layout: {
            width: 'auto',
            marginLeft: theme.spacing.unit * 3,
            marginRight: theme.spacing.unit * 3,
            [theme.breakpoints.up(1280 + theme.spacing.unit * 3 * 2)]: {
                width: 1280,
                marginLeft: 'auto',
                marginRight: 'auto',
                marginTop: '20px',
            },
            mainGrid: {
                marginTop: theme.spacing.unit * 3,
            },
            markdown: {
                padding: `${theme.spacing.unit * 3}px 0`,
            },
        },
        form: {
            width: '100%', // Fix IE11 issue.
            marginTop: theme.spacing.unit,
        },
        submit: {
            marginTop: theme.spacing.unit * 3,
        },
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing.unit,
            marginRight: theme.spacing.unit,
            width: 200,
        },
        formControl: {
            marginLeft: theme.spacing.unit,
            marginRight: theme.spacing.unit,
            width: 200,
        },
    }),

    projectListPageStyle: theme => ({
        layout: {
            width: 'auto',
            marginLeft: theme.spacing.unit * 3,
            marginRight: theme.spacing.unit * 3,
            [theme.breakpoints.up(1280 + theme.spacing.unit * 3 * 2)]: {
                width: 1280,
                marginLeft: 'auto',
                marginRight: 'auto',
                marginTop: '20px',
            },
        },
        cardGrid: {
            padding: `${theme.spacing.unit * 8}px 0`,
        },
        category: {
            marginBottom: "10px",
        },
        filter: {
            minWidth: "140px",
            marginLeft: "7px",
            marginRight: "7px"
        },
        button: {
            marginLeft: "20px",
            marginRight: "20px"
        },
        fundsFilter: {
            width: "100px"
        }
    }), 

    projectTableStyle: theme => ({
        table: {
            minWidth: 700,
        },
    }),

    donateDialogStyle: theme => ({
        form: {
            width: '100%', // Fix IE11 issue.
            marginTop: theme.spacing.unit,
        },
        submit: {
            marginTop: theme.spacing.unit * 3,
        },
    }),
    
    createCategoryPageStyle: theme => ({
        form: {
            width: '100%', // Fix IE11 issue.
            marginTop: theme.spacing.unit,
        },
        submit: {
            marginTop: theme.spacing.unit * 3,
        },
    }),

    adminDashboardPageStyle: theme => ({
        layout: {
            width: 'auto',
            marginLeft: theme.spacing.unit * 3,
            marginRight: theme.spacing.unit * 3,
            [theme.breakpoints.up(1280 + theme.spacing.unit * 3 * 2)]: {
                width: 1280,
                marginLeft: 'auto',
                marginRight: 'auto',
                marginTop: '20px',
            },
        },
        table: {
            minWidth: 500,
        },
        tableWrapper: {
            overflowX: 'auto',
        },
    }),

    accountDashboardPageStyle: theme => ({
        layout: {
            width: 'auto',
            marginLeft: theme.spacing.unit * 3,
            marginRight: theme.spacing.unit * 3,
            [theme.breakpoints.up(1280 + theme.spacing.unit * 3 * 2)]: {
                width: 1280,
                marginLeft: 'auto',
                marginRight: 'auto',
                marginTop: '20px',
            },
        },
        table: {
            minWidth: 500,
        },
        tableWrapper: {
            overflowX: 'auto',
        },
        largeIcon: {
            width: 90,
            height: 90,
        },
    }),
    
    searchBarStyle:  theme => ({
        root: {
            flexGrow: 1,
            height: 50,
            width: 200,
        },
        input: {
            display: 'flex',
            padding: 0,
        },
        valueContainer: {
            display: 'flex',
            flexWrap: 'wrap',
            flex: 1,
            alignItems: 'center',
        },
        chip: {
            margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
        },
        chipFocused: {
            backgroundColor: emphasize(
                theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
                0.08,
            ),
        },
        noOptionsMessage: {
            padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
        },
        singleValue: {
            fontSize: 16,
        },
        placeholder: {
            position: 'absolute',
            left: 2,
            fontSize: 16,
        },
        paper: {
            position: 'absolute',
            zIndex: 1,
            marginTop: theme.spacing.unit,
            left: 0,
            right: 0,
        },
        divider: {
            height: theme.spacing.unit * 2,
        },
    }) 

}