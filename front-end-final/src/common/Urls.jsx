export default {
    account: {
        entity: "/api/account/entity",
        list: "/api/account/list"
    },
    category: {
        entity: "/api/category/entity",
        list: "/api/category/list"
    },
    project: {
        entity: "/api/project/entity",
        list: "/api/project/list"
    },
    transfer: {
        entity: "/api/transfer/entity",
        list: "/api/transfer/list"
    },
    email: {
        entity: "/api/email/entity",
        list: "/api/email/list"
    },

}