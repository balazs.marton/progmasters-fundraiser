import React from 'react';
import { Redirect } from 'react-router-dom'
import PropTypes from 'prop-types';
import Select from 'react-select';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import NoSsr from '@material-ui/core/NoSsr';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import Styles from './Styles';
import Urls from './Urls';
import axios from 'axios';

function NoOptionsMessage(props) {
    return (
        <Typography
            color="textSecondary"
            className={props.selectProps.classes.noOptionsMessage}
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    );
}

function inputComponent({ inputRef, ...props }) {
    return <div ref={inputRef} {...props} />;
}

function Control(props) {
    return (
        <TextField
            fullWidth
            InputProps={{
                inputComponent,
                inputProps: {
                    className: props.selectProps.classes.input,
                    inputRef: props.innerRef,
                    children: props.children,
                    ...props.innerProps,
                },
            }}
            {...props.selectProps.textFieldProps}
        />
    );
}

function Option(props) {
    return (
        <MenuItem
            buttonRef={props.innerRef}
            selected={props.isFocused}
            component="div"
            style={{
                fontWeight: props.isSelected ? 500 : 400,
            }}
            {...props.innerProps}
        >
            {props.children}
        </MenuItem>
    );
}

function Placeholder(props) {
    return (
        <Typography
            color="textSecondary"
            className={props.selectProps.classes.placeholder}
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    );
}

function SingleValue(props) {
    return (
        <Typography className={props.selectProps.classes.singleValue} {...props.innerProps}>
            {props.children}
        </Typography>
    );
}

function ValueContainer(props) {
    return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
}


function Menu(props) {
    return (
        <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
            {props.children}
        </Paper>
    );
}

const components = {
    Control,
    Menu,
    NoOptionsMessage,
    Option,
    Placeholder,
    SingleValue,
    ValueContainer,
};

class IntegrationReactSelect extends React.Component {

    state = {
        single: { value: null, isRedirect: false },
        suggestions: []
    };

    handleChange = value => {
        console.log(value);
        this.setState({
            single: { value: value, isRedirect: true }
        });
    };

    componentDidMount() {
        let suggestions = [];
        let projectMap = {};
        axios.get(Urls.project.list + "/inMap")
            .then(response => {
                projectMap = response.data.projectMap;
                Object.entries(projectMap).map(([key, value]) => {
                    suggestions.push({ label: key, value: value });
                })
                this.setState({
                    suggestions: suggestions
                });
            })
            .catch(error => {
                console.log("Logging error in componentDidMount:", error);
            });
    }

    renderRedirect = () => {
        if (this.state.single.isRedirect) {
            let updateSingle = { ...this.state.single };
            updateSingle.isRedirect = false;
            console.log("redirect!", updateSingle)
            this.setState({
                single: updateSingle
            }
            );
            return <Redirect to={"/redirectSearchBar/" + this.state.single.value.value} />
        }
    }

    render() {
        const { classes, theme } = this.props;

        const selectStyles = {
            input: base => ({
                ...base,
                color: theme.palette.text.primary,
                '& input': {
                    font: 'inherit',
                },
            }),
        };

        return (
            <div className={classes.root}>
                <NoSsr>
                    <Select
                        classes={classes}
                        styles={selectStyles}
                        options={this.state.suggestions}
                        components={components}
                        value={this.state.single}
                        onChange={this.handleChange}
                        placeholder="Search a project by Name"
                    />
                </NoSsr>
                {this.renderRedirect()}
            </div>
        );
    }
}

IntegrationReactSelect.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(Styles.searchBarStyle, { withTheme: true })(IntegrationReactSelect)