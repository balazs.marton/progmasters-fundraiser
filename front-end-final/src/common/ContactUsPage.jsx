import React from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Divider from "@material-ui/core/Divider";

const styles = theme => ({
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1280 + theme.spacing.unit * 3 * 2)]: {
            width: 1280,
            marginLeft: 'auto',
            marginRight: 'auto',
            marginTop: '20px',
        },
    },
});

function ContactUsPage(props) {
    const { classes } = props;

    return (
        <React.Fragment>
            <CssBaseline />
            <main>

                <div className={classes.layout}>
                    <Typography variant="headline" color="textPrimary" gutterBottom>
                        Contact us
                    </Typography>
                    <Divider/>
                    <br/>
                    <Typography  component="span" >

                        This is the contact us page of the company.
                    </Typography>
                </div>
            </main>

        </React.Fragment>
    );
}

ContactUsPage.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ContactUsPage);