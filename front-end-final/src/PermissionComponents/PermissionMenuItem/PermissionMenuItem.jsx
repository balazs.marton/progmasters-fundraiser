import React, { Fragment } from 'react';
import MenuItem from '@material-ui/core/MenuItem';

function PermissionMenuItem({role, children, ...rest}) {
    let user = JSON.parse(localStorage.getItem('user'));
    let isValid = role.includes(user.role);
    // console.log("isValid for " + props.label + " PermissionButton? ", isValid);
    return (
        <Fragment>
            {isValid ?
                <Fragment>
                    <MenuItem {...rest}>{children}</MenuItem>
                </Fragment>
                : <Fragment></Fragment>}
        </Fragment>
    )
}


export default PermissionMenuItem;