import React, { Fragment } from 'react';
import Button from '@material-ui/core/Button';

function PermissionButton({role, children, ...rest}) {
    let user = JSON.parse(localStorage.getItem('user'));
    let isValid = role.includes(user.role);
    // console.log("isValid for " + props.label + " PermissionButton? ", isValid);
    return (
        <Fragment>
            {isValid ?
                <Fragment>
                    <Button {...rest}>{children}</Button>
                </Fragment>
                : <Fragment></Fragment>}
        </Fragment>
    )
}


export default PermissionButton;