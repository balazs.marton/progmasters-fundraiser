import React, { Fragment } from 'react';
import IconButton from '@material-ui/core/IconButton';

function PermissionIconButton({role, children, ...rest}) {
    let user = JSON.parse(localStorage.getItem('user'));
    let isValid = role.includes(user.role);
    // console.log("isValid for " + props.label + " PermissionButton? ", isValid);
    return (
        <Fragment>
            {isValid ?
                <Fragment>
                    <IconButton {...rest}>{children}</IconButton>
                </Fragment>
                : <Fragment></Fragment>}
        </Fragment>
    )
}


export default PermissionIconButton;