import React, {Component} from 'react';
import './App.css';
import {BrowserRouter} from 'react-router-dom';
import Layout from './common/Layout';
import {createMuiTheme} from '@material-ui/core/styles';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import ScrollToTop from "./common/ScrollToTop";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#bbdefb',
        },
        secondary: {
            main: '#ffab40',
        },
    },
});

class App extends Component {

    render() {
        return (
            <BrowserRouter>
                <MuiThemeProvider theme={theme}>
                    <ScrollToTop>
                        <div className="App">
                            <Layout/>
                        </div>
                    </ScrollToTop>
                </MuiThemeProvider>
            </BrowserRouter>
        );
    }
}

export default App;
